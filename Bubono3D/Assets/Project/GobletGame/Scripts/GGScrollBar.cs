﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GGScrollBar : MonoBehaviour
{
    Scrollbar mScrollbar;

    private float mSpeed = 0.5f;
    private GameObject mTargetPlayer;

    private bool mScrollBarON;

    // Start is called before the first frame update
    void Start()
    {
        mScrollbar = GetComponent<Scrollbar>();
        mScrollBarON = true;
        //InvokeRepeating("updateScrollbarValue", 1.0f,  1.0f);
        setScrollbarLocation();
    }

    private void setScrollbarLocation()
    {
        GameObject goblet = GameObject.FindGameObjectWithTag("GG_Goblet");

        Vector3 targetPos = Camera.main.WorldToScreenPoint(goblet.transform.position + new Vector3(0.0f, -1.0f, 0.0f));
        transform.position = targetPos;
    }

    public void setTargetPlayer(GameObject targetPlayer)
    {
        mTargetPlayer = targetPlayer;
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(mScrollBarON)
            updateScrollbarValue();
    }

    /*private void LateUpdate()
    {
        if (mTargetPlayer == null)
            return;

        Vector3 targetPos = Camera.main.WorldToScreenPoint(mTargetPlayer.transform.position + new Vector3(0.0f, 2.0f, 0.0f));
        transform.position = targetPos;
    }*/

    private void updateScrollbarValue()
    {
        if(mScrollbar.value <= 0.0f)
        {
            mSpeed = Mathf.Abs(mSpeed);
        }
        else if(mScrollbar.value >= 1.0f)
        {
            mSpeed = mSpeed * -1.0f;
        }

        mScrollbar.value += mSpeed * Time.deltaTime;
    }

    public void startScrollbar()
    {
        mScrollBarON = true;
    }

    public void stopScrollbar()
    {
        mScrollBarON = false;
    }

    public float getScrollbarValue()
    {
        return mScrollbar.value;
    }

    public void SetScrollbarSpeed(float speed)
    {
        mSpeed = speed;
    }
}

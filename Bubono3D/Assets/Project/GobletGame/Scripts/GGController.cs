﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

public class GGController : MonoBehaviour
{

    /// <summary>
    /// BBGoblet mGoblet => Référence vers le gobelet surlequel chaque controleur peut agir
    /// </summary>
    [SerializeField] protected BBGoblet mGoblet;

    /// <summary>
    /// bool mIsMyTurn => Ce flag est activé quand le controleur possède l'initiative du tour courant
    /// </summary>
    [SerializeField] protected bool mIsMyTurn;

    /// <summary>
    ///  GameObject mCharacter => Référence vers le personnage lié au controleur. Ce lien permet d'activé les animations lié à chaque personnage
    /// </summary>
    [SerializeField] protected GameObject mCharacter;

    /// <summary>
    /// GameObject mImpact => Référence vers un feedback d'impact qui est activé dès lors que le controleur a agit sur le gobelet
    /// </summary>
    [SerializeField] protected GameObject mImpact;

    /// <summary>
    /// GameObject mIcon => Référence vers un sélecteur qui est présent sur chaque personnage. L'icone doit juste indiquer quel personnage possède l'initiative. Elle n'est active qu'à ce moment là
    /// </summary>
    [SerializeField] protected GameObject mIcon;

    [SerializeField] protected AudioClip mImpactSound;

    /// <summary>
    /// int mTeamNumber => Chaque controleur possède un numéro d'équipe qui sert à l'identifier notamment auprès du publique qui le supporte. Le public réagit alors en fonction des réussites et échecs du joueur qui leur est lié 
    /// </summary>
    public int mTeamNumber;

    /// <summary>
    /// Gestionnaire d'animation du personnage. Est utilisé pour gérer les animations du personnage
    /// </summary>
    protected Animator mCharacterAnimator;

    /// <summary>
    /// string mPlayerName => Chaque personnage possède un nom qui sera affiché dans la GUI.
    /// </summary>
    public string mPlayerName;

    /// <summary>
    /// int mScore => Chaque personnage possède un score qui permettra de déterminer sa victoire ou sa défaite
    /// </summary>
    private int mScore;

    public virtual void rotateGoblet()
    {
       
    }

    /// <summary>
    /// Start() => Initialise le controleur :
    ///     - Désactive l'icone sur le personnage
    ///     - Récupère le gestionnaire d'animation du personnage
    /// </summary>
    protected void Start()
    {

        if (mIsMyTurn)
            mIcon.SetActive(true);

        mCharacterAnimator = mCharacter.GetComponent<Animator>();
    }

    /// <summary>
    /// activateTurn() => Active le tour du personnage
    /// </summary>
    public void activateTurn()
    {
        mIsMyTurn = true;
        mIcon.SetActive(true);

        Invoke("proceedToNoActionRotateGoblet", 10f);
    }

    /// <summary>
    /// disableTurn() => désactive le tour du personnage
    /// </summary>
    public void disableTurn()
    {
        mIsMyTurn = false;
        mIcon.SetActive(false);

        CancelInvoke("proceedToNoActionRotateGoblet");
    }

    /// <summary>
    /// increaseScore() => Mise à jour du score lié au controleur :
    ///     - D'abord on incrémente le score
    ///     - On affiche le score dans le paramètre scoreText passé en référence
    /// </summary>
    /// <param name="scoreText">UnityEngine.UI.Text scoreText => référence vers un UI qui va afficher le score de chaque personnage</param>
    public void increaseScore(UnityEngine.UI.Text scoreText)
    {
        mScore++;
        scoreText.text = mPlayerName + ":" +mScore.ToString();
    }

    /// <summary>
    /// activateDefeatAnimation() => active l'animation de défaite du personnage lié au controleur
    /// </summary>
    public void activateDefeatAnimation()
    {
        mCharacterAnimator.SetBool("DefeatON", true);
    }

    /// <summary>
    ///  activateWinDanceAnimation() => active l'animation de victoire du personnage lié au controleur 
    /// </summary>
    public void activateWinDanceAnimation()
    {
        mCharacterAnimator.SetBool("WinDanceON", true);
    }

    /// <summary>
    /// getScore() => récupère le score lié au controleur et au personnage
    /// </summary>
    /// <returns> int mScore => Retourne la valeur du paramètre mScore</returns>
    public int getScore()
    {
        return mScore;
    }

    protected void proceedToNoActionRotateGoblet()
    {
        rotateGoblet();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Hellmade.Sound;

public class GGPlayerController : GGController
{
    /// <summary>
    /// float mRotateSpeedMin => vitesse de rotation minimum lors de la projection du verre. La vitesse de rotation est une valeur aléatoire entre une vitesse min et max
    /// </summary>
    [SerializeField] float mRotateSpeedMin = 1.0f;

    /// <summary>
    /// float mRotateSpeedMax => vitesse de rotation maximum lors de la projection du verre. La vitesse de rotation est une valeur aléatoire entre une vitesse min et max
    /// </summary>
    [SerializeField] float mRotateSpeedMax = 20.0f;

    [SerializeField] float mRotationBonusMin = 1.0f;
    [SerializeField] float mRotationMalusMin = 1.0f;
    [SerializeField] float mRotationBonusMax = 1.0f;
    [SerializeField] float mRotationMalusMax = 1.0f;
    [SerializeField] float mScrollbarSpeed = 1.0f;

    private GGScrollBar mScrollbar;
    private float mScrollbarValue;
    private bool mIsScrollbarMoveON;

    private int mNoActionTimer = 10;

    private GGGameManager mGameManager;

    private new void Start()
    {
        base.Start();

        mPlayerName = PlayerPrefs.GetString("PlayerPseudo", "Player");
        mScrollbar = GameObject.FindGameObjectWithTag("GG_Scrollbar").GetComponent<GGScrollBar>();
        //mScrollbar.setTargetPlayer(mGoblet);
        mScrollbar.SetScrollbarSpeed(mScrollbarSpeed);
        mScrollbar.gameObject.SetActive(false);
        mIsScrollbarMoveON = false;

        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GGGameManager>();


    }

    private void Update()
    {
        if(mScrollbar != null && mGameManager.mGameState == GGGameParametes.eGameState.INGAME)
        {
            if (Input.GetMouseButtonDown(0) && mIsMyTurn)
            {
                mScrollbar.stopScrollbar();
                mScrollbarValue = mScrollbar.getScrollbarValue();
                rotateGoblet();
                mIsScrollbarMoveON = false;
                mScrollbar.gameObject.SetActive(false);
            }
            else if (mIsMyTurn && !mIsScrollbarMoveON)
            {
                mScrollbar.gameObject.SetActive(true);
                mScrollbar.startScrollbar();
                mIsScrollbarMoveON = true;
            }
        }
        
    }

    /// <summary>
    /// rotateGoblet() => Le controleur fait tourner le gobelet 
    ///     - Lorsque le joueur clique sur le gobelet la fonction rotateGoblet du controleur est appelée
    ///     - Une animation du personnage tapant sur la table est activé
    ///     - Ensuite un effet d'impact est déclenché
    ///     - L'écran vibre alors
    ///     - La fonction rotateGoblet du Gobelet est appelée
    ///     - Enfin on désactive le tour du controleur
    /// </summary>
    public override void rotateGoblet()
    {
        if (mIsMyTurn)
        {
            if (mScrollbar.gameObject.activeSelf)
            {
                mScrollbar.stopScrollbar();
                mIsScrollbarMoveON = false;
                mScrollbar.gameObject.SetActive(false);
            }
            mCharacterAnimator.SetBool("HitON", true);
            DOVirtual.DelayedCall(0.5f, () =>
            {
                EazySoundManager.PlaySound(mImpactSound);
                mImpact.GetComponent<ParticleSystem>().Play();
                Camera.main.transform.DOShakePosition(0.5f, 0.1f);
                mGoblet.rotateGoblet(
                    computeRotationBonus(mRotateSpeedMin, mRotationBonusMin, mRotationMalusMin),
                    computeRotationBonus(mRotateSpeedMax, mRotationBonusMax, mRotationMalusMax));
                mCharacterAnimator.SetBool("HitON", false);
            });

            disableTurn();
        }
    }

    private float computeRotationBonus(float rotationValue, float bonusMultiplier, float malusMultiplier)
    {
        
       if(mScrollbarValue >= 0.45f && mScrollbarValue <= 0.55f)
        {
            rotationValue *= bonusMultiplier;
        }
       else if(mScrollbarValue >0.55f && mScrollbarValue < 0.75f || mScrollbarValue > 0.25f && mScrollbarValue < 0.45f)
        {
        }
       else
        {
            rotationValue *= malusMultiplier;
        }

        return rotationValue;
    }
}


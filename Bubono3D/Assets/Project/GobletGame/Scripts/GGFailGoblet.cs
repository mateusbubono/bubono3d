﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GGFailGoblet : MonoBehaviour
{
    /// <summary>
    /// BBGoblet mGoblet => Référence vers le Gobelet auquel appartient le trigger de défaite
    /// </summary>
    [SerializeField] BBGoblet mGoblet;

    /// <summary>
    /// OnTriggerEnter() => Lorsque le gobelet tombe sur le côté ou le dessus, le trigger de défaite s'active et notifie le Gobelet de l'échec. Le Gobelet informe alors le GameManager de l'échec du lancé
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if(mGoblet.mRotateON)
        {
            mGoblet.onRotateFail();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class GGAIController : GGController
{

    new void Start()
    {
        base.Start();
    }

    /// <summary>
    /// L'ia va tenter d'intéragir avec le gobelet à chaque frame
    /// - Elle ne pourra le faire que lorsque son tour sera arrivé
    /// - A ce moment elle lance l'animation de tape du personnage sur la table
    /// - Puis elle appelle la fonction rotateGoblet de la classe
    /// </summary>
    private void Update()
    {
        if (mIsMyTurn)
        {
            DOVirtual.DelayedCall(0.5f, () =>
            {
                mCharacterAnimator.SetBool("HitON", true);
                Invoke("rotateGoblet", 0.5f);
                disableTurn();
            });
            mIsMyTurn = false; // La ligne est nécessaire pour que l'icône du joueur s'affiche plus longuement sur le personnage IA
            
        }
       
    }

    /// <summary>
    /// rotateGoblet() => controle l'action de déclencher la rotation du gobelet
    ///     - Déclenche un effet d'impact sur la table
    ///     - Secoue la caméra
    ///     - Appelle la fonction de rotation du gobelet
    /// </summary>
    public override void rotateGoblet()
    {
        EazySoundManager.PlaySound(mImpactSound);
        mImpact.GetComponent<ParticleSystem>().Play();
        Camera.main.transform.DOShakePosition(0.5f, 0.1f);
        mCharacterAnimator.SetBool("HitON", false);
        mGoblet.rotateGoblet();
    }

}

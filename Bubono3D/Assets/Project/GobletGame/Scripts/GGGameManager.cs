﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Hellmade.Sound;

public class GGGameManager : MonoBehaviour
{

    /// <summary>
    /// bool mMainMenuON => Indique que le jeu se trouve actuellement dans le menu principal (true) oui non
    /// </summary>
    [SerializeField] bool mMainMenuON = false;

    /// <summary>
    ///  GGGameParametes.eGameState mGameState => Contient l'état de la partie
    /// </summary>
    public GGGameParametes.eGameState mGameState = GGGameParametes.eGameState.START;

    /// <summary>
    /// string[] mPlayerNameList => Liste contenant le nom des joueurs de type IA
    /// </summary>
    [SerializeField] string[] mPlayerNameList;

    /// <summary>
    /// Text mTurnResult => Affiche le résultat du lancé au joueur
    /// </summary>
    [SerializeField] Text mTurnResult;

    /// <summary>
    /// Text mTimerText => Affiche le temps restant de la partie
    /// </summary>
    [SerializeField] Text mTimerText;

    /// <summary>
    /// int mTimer => Permet de calculer le temps restant de la partie
    /// </summary>
    [SerializeField] int mTimer = 60;

    /// <summary>
    /// GameObject[] mLevels => La liste des niveaux 
    /// </summary>
    [SerializeField] GameObject[] mLevels;

    /// <summary>
    /// int mCurrentLevel => Indice du niveau courant. Si la valeur est -1 c'est le jeu qui gère cet index. Sinon le niveau indiqué est celui qui sera chargé
    /// </summary>
    [SerializeField] int mCurrentLevel;

    /// <summary>
    ///  GameObject mPanel => Panel qui affiche :
    ///     - Le nom des personnages qui s'affrontent
    ///     - Le temps restant de la partie
    ///     - Les points marqué par chaque personnage
    /// </summary>
    [SerializeField] GameObject mPanel;

    /// <summary>
    /// GameObject mUpPanelPoint => Une position hors de l'écran qui va permettre au panel de s'y déplacer (animation de feedback du panel)
    /// </summary>
    [SerializeField] GameObject mUpPanelPoint;

    /// <summary>
    /// GameObject mDownPanelPoint =>  Une position dans l'écran qui va permettre au panel de s'y déplacer (animation de feedback du panel) pour revenir dans l'écran
    /// </summary>
    [SerializeField] GameObject mDownPanelPoint;

    /// <summary>
    ///  GameObject mWinPanel => Affichage du panneau en cas de victoire
    /// </summary>
    [SerializeField] GameObject mWinPanel;

    /// <summary>
    /// GameObject mFailPanel => Permet l'affichage du panneau en cas de défaite
    /// </summary>
    [SerializeField] GameObject mFailPanel;

    /// <summary>
    /// GameObject mSuccessFeedback => Animation de succès qui est joué à chaque fois qu'un joueur réussi un lancé
    /// </summary>
    GameObject mSuccessFeedback;

    /// <summary>
    /// List<GGController> mPlayerList => La liste des controller dans la partie
    /// </summary>
    List<GGController> mPlayerList;

    /// <summary>
    ///  List<GGPublic> mPublicList => La liste des publiques présent dans le niveau
    /// </summary>
    List<GGPublic> mPublicList;

    /// <summary>
    /// List<Text> mPlayerScoreList => Liste des scores. Elle seront mise à jours à chaque lancé et le résultat sera affiché à l'écran
    /// </summary>
    List<Text> mPlayerScoreList;

    /// <summary>
    /// int mCurrentPlayer => indice du joueur courant qui doit jouer son tour
    /// </summary>
    private int mCurrentPlayer;

    /// <summary>
    /// GameObject[] mCotillonList => La liste des animations de cotillons qui seront joué à la fin de la partie
    /// </summary>
    private GameObject[] mCotillonList;

    /// <summary>
    /// getARandomName() => récupère dans la liste mPlayerNameList un nom aléatoire qui sera attribué à l'IA adverse
    /// </summary>
    /// <returns>String randomName => un nom aléatoire </returns>
    public string getARandomName()
    {
        return mPlayerNameList[Mathf.RoundToInt(Random.Range(0, mPlayerNameList.Length - 1))];
    }


    /// <summary>
    /// getPublicInGame() => Récupère dans une liste l'ensemble des publics présent sur la scène 
    /// </summary>
    private void getPublicInGame()
    {
        mPublicList = new List<GGPublic>();
        GameObject[] publicTab = GameObject.FindGameObjectsWithTag("GG_Public");

        foreach(GameObject publicGO in publicTab)
        {
            mPublicList.Add(publicGO.GetComponent<GGPublic>());
        }
    }


    /// <summary>
    ///  gameOver() => Fonction appelée lors de la fin de la partie :
    ///     - Change l'état de la partie à GAMEOVER
    ///     - Empêche les joueurs d'agir en leur coupant tout possibilité de jouer
    ///     - Stoppe les animations DOTween
    ///     - Recherche le joueur gagnant et affiche son nom et son score
    ///     - Déclenche des animations de fin de parties pour les joueurs (dance ou déception) et déclenche l'effet de cotillons
    ///     - Déclenche la procédure de victoire ou défaite si le gagnant est un joueur humain ou non
    /// </summary>
    private void gameOver()
    {
        mGameState = GGGameParametes.eGameState.GAMEOVER;
        stopAllPlayers();

        DOTween.KillAll();

        GGController winner = lookForTheWinner();

        mTurnResult.gameObject.SetActive(true);
        mTurnResult.text = "Winner is " + winner.mPlayerName + " with " + winner.getScore() + " points";

        animateEndGame(winner);

        showCotillons();

        if(winner.GetComponent<GGPlayerController>() == null)
        {
            fail();
        }
        else
        {
            win();
        }
    }

    /// <summary>
    /// win() => en cas de victoire un panel de victoire s'affiche
    /// </summary>
    private void win()
    {
        Invoke("showWinPanel", 1.0f);
    }

    /// <summary>
    /// fail() => en cas de défaite un panel de défaite s'affiche
    /// </summary>
    private void fail()
    {
        Invoke("showFailPanel", 1.0f);
    }

    /// <summary>
    /// lookForTheWinner() => la fonction va parcourir tous les scores des joueurs dans la liste mPlayerScoreList afin de rechercher le gagnants
    ///     - on regarde dans mPlayerScoreList le meilleurs score
    ///     - on récupère le joueur possédant le meilleurs score 
    /// </summary>
    /// <returns>GGController winner => le controller du joueur gagnant</returns>
    private GGController lookForTheWinner()
    {
        GGController winner = null;
        int maxScore = 0;
        int winnerIndex = 0;
        for(int i = 0; i< mPlayerScoreList.Count; i++)
        {
            int score = mPlayerList[i].getScore();
            if (winner == null || score > maxScore)
            {
                maxScore = score;
                winner = mPlayerList[i];
                winnerIndex = i;
            }
        }

        //Je ne me souviens plus de l'utilité de cette ligne
        //mPlayerScoreList[winnerIndex].transform.GetChild(0).gameObject.SetActive(true);
        return winner;
    }

    /// <summary>
    /// animateEndGame() => Lance les animations de fin de partie
    ///     - Lance l'animation des joueurs en fonction de la victoire ou défaite
    ///     - Lance l'animation du publique en fonction du joueur qui à gagné ou perdu
    /// </summary>
    /// <param name="winner">GGController winner => Le controller du joueur qui a gagné la partie</param>
    private void animateEndGame(GGController winner)
    {
        foreach(GGController player in mPlayerList)
        {
            if(player == winner)
            {
                player.activateWinDanceAnimation();
            }
            else
            {
                player.activateDefeatAnimation();
            }
        }

        foreach(GGPublic publicTeam in mPublicList)
        {
            if(publicTeam.mTeamNumber == winner.mTeamNumber)
            {
                publicTeam.onTeamSuccess(true);
            }
            else
            {
                publicTeam.onTeamFail(true);
            }
        }
    }

    /// <summary>
    /// stopAllPlayers() => stoppe toute possibilité d'action à tous les joueurs
    /// </summary>
    private void stopAllPlayers()
    {
        foreach(GGController player in mPlayerList)
        {
            player.disableTurn();
        }
    }


    /// <summary>
    /// getPlayersInGame() => Récupèration de tous les joueurs présent dans la partie
    ///     - On commence par récupérer tous les joueurs (Les controleurs des joueurs et les controleurs des AI)
    ///     - On récupère également les scores de chaque joueur
    ///     - Pour chaque joueur on désactive le tour on ajoute le joueur à la liste des joueurs du GameManager, on associe le pseudo du joueur
    ///     - Pour chaque IA on désactive le tour et on ajoute l'IA à la liste des joueurs du GameManager. On associe un pseudo aléatoire à l'IA
    ///     - On affiche les scores (à part sur l'écran principal)
    ///     - On donne le tour au premier joueur de la liste des joueurs
    /// </summary>
    private void getPlayersInGame()
    {
       GameObject[] playerList =  GameObject.FindGameObjectsWithTag("GG_PlayerController");
       GameObject[] aiList =  GameObject.FindGameObjectsWithTag("GG_AIController");

        GameObject[] playerScoreTexts = GameObject.FindGameObjectsWithTag("GG_PlayerScore");

        foreach(GameObject player in playerList)
        {
            GGPlayerController playerController = player.GetComponent<GGPlayerController>();
            mPlayerList.Add(playerController);
            playerController.disableTurn();
            playerController.mPlayerName = PlayerPrefs.GetString("PlayerPseudo", "Player"); //Attention valable que pour un seul joueur
        }

        foreach (GameObject ai in aiList)
        {
            GGAIController aiController = ai.GetComponent<GGAIController>();
            mPlayerList.Add(aiController);
            aiController.disableTurn();
            aiController.mPlayerName = getARandomName(); // Attention valable que pour une seule IA
        }

        if(mMainMenuON == false)
        {
            for(int i = 0; i< playerScoreTexts.Length; i++)
            {
                mPlayerScoreList.Add(playerScoreTexts[i].GetComponent<Text>());
                playerScoreTexts[i].GetComponent<Text>().text = mPlayerList[i].mPlayerName + ": 0";
                playerScoreTexts[i].transform.GetChild(0).gameObject.SetActive(false);
            }
        }

        giveTurnToPlayer();
    }

    /// <summary>
    /// onPlayerEndTurn() => Appelée quand le joueur à terminé son tours :
    ///     - Il passe alors au joueur suivant
    ///     - Si le timer de la partie est terminé le jeu est interrompue
    /// </summary>
    public void onPlayerEndTurn()
    {
        if (mGameState == GGGameParametes.eGameState.INGAME)
        {
            if (mTimer <= 0)
            {
                CancelInvoke("updateTimer");
                gameOver();
            }
            else
            {
                mSuccessFeedback.SetActive(false);
                mCurrentPlayer++;
                mCurrentPlayer %= mPlayerList.Count;
                giveTurnToPlayer();
            }
        }
    }

    /// <summary>
    /// giveTurnToPlayer() => active le tour du joueur suivant dans la liste des joueurs
    /// </summary>
    private void giveTurnToPlayer()
    {
        mPlayerList[mCurrentPlayer].activateTurn();

        if(mMainMenuON == false) //Le score du joueur actif s'affiche en jaune au lieu de blanc pour le joueur inactif
            mPlayerScoreList[mCurrentPlayer].color = Color.yellow;
    }

    /// <summary>
    /// disableAllIcons() => désactive les icones des personnages (notamment et le plus important ceux qui sont dans le publique)
    /// </summary>
    private void disableAllIcons()
    {
        GameObject[] iconList =  GameObject.FindGameObjectsWithTag("GG_Icon");

        foreach(GameObject icon in iconList)
        {
            icon.SetActive(false);
        }
    }

    /// <summary>
    /// Awake() => Appelée pour initialisé le GameManager :
    ///     - Désactive le son si on se trouve dans le menu principal
    ///     - Charge le niveau courant
    ///     - Cache les cotillons pour la fin de la partie
    ///     - Cache le panel de défaite et de victoire
    ///     - Désactive les icônes aux dessus de tous les GameObjects qui sont des characters
    ///     - Récupère le feedback de réussite d'un lancé et le désactive
    ///     - Initialise la liste des joueurs et la liste des scores
    ///     - Récupère la liste des joueurs et le publique dans la scène
    ///     - Passe l'état de la partie à INGAME
    ///     - Initialise l'update du timer de la partie
    /// </summary>
    void Awake()
    {
        int musicON = PlayerPrefs.GetInt("MusicON", 1);
        if(mMainMenuON || musicON == 0)
        {
            EazySoundManager.GlobalSoundsVolume = 0;
        }
        else
        {
            EazySoundManager.GlobalSoundsVolume = 1;

        }

        loadLevel();

        mCotillonList = GameObject.FindGameObjectsWithTag("Cotillon");
        hideCotillons();
        hideFailPanel();
        hideWinPanel();
        disableAllIcons();
        mSuccessFeedback = GameObject.FindGameObjectWithTag("GG_SuccessFeedback");
        mSuccessFeedback.SetActive(false);
        mPlayerList = new List<GGController>();
        mPlayerScoreList = new List<Text>();
        getPlayersInGame();
        getPublicInGame();

        if (mMainMenuON == false)
        {
            mTurnResult.text = "Get Ready";
        }

        DOVirtual.DelayedCall(3.0f, () =>
        {
            mGameState = GGGameParametes.eGameState.INGAME;
            mCurrentPlayer = 0;
            
            if(mMainMenuON == false)
            {
                mTurnResult.gameObject.SetActive(false);
                InvokeRepeating("updateTimer", 0.3f, 1.0f);
            }
            
        }).SetLink(gameObject);
       
    }

    /*void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKey(KeyCode.R))
            restart();
        #endif
    }*/

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void nextLevel()
    {
        mCurrentLevel = (mCurrentLevel + 1) % mLevels.Length;
        PlayerPrefs.SetInt("currentLevel", mCurrentLevel);
        restart();
    }

    void loadLevel()
    {
        if (mCurrentLevel == -1)
        {
            mCurrentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        }
        GameObject level = Instantiate(mLevels[mCurrentLevel]);
        level.transform.parent = transform;

    }

    /// <summary>
    /// proceedToTeamSuccess() => gère les réaction du public au réussite ou échec du joueur qu'il supporte
    /// </summary>
    private void proceedToTeamSuccess()
    {
        foreach (GGPublic publicTeam in mPublicList)
        {
            if (publicTeam.mTeamNumber == mPlayerList[mCurrentPlayer].mTeamNumber)
            {
                publicTeam.onTeamSuccess();
            }
            else
            {
                publicTeam.onTeamFail();
            }
        }
    }

    /// <summary>
    /// proceedToTeamSuccess() => gère les réaction du public au réussite ou échec du joueur qu'il supporte
    /// </summary>
    private void proceedToTeamFailure()
    {
        foreach (GGPublic publicTeam in mPublicList)
        {
            if (publicTeam.mTeamNumber == mPlayerList[mCurrentPlayer].mTeamNumber)
            {
                publicTeam.onTeamFail();
            }
            else
            {
                publicTeam.onTeamSuccess();
            }
        }
    }

    /// <summary>
    ///     onRotateSuccess() => appelé lors du succès de rotation du gobelet :
    ///         - Affiche un commentaire de succès
    ///         - Active un feedback de succès
    ///         - Active l'animation de succès du public
    ///         - Augmente le score
    /// </summary>
    public void onRotateSuccess()
    {
        if(mMainMenuON == false)
        {
            showTurnResult("Well Done !!!");
            mPlayerScoreList[mCurrentPlayer].color = Color.white;
            mPlayerList[mCurrentPlayer].increaseScore(mPlayerScoreList[mCurrentPlayer]);
        }

        mSuccessFeedback.SetActive(true);
        proceedToTeamSuccess();
    }

    /// <summary>
    /// onRotateFail() => appelé lors de l'échec de rotation du gobelet
    ///     - affiche un commentaire d'échec
    ///     - active l'animation d'échec du public
    /// </summary>
    public void onRotateFail()
    {
        if(mMainMenuON == false)
        {
            showTurnResult("Try Again !!!");
            mPlayerScoreList[mCurrentPlayer].color = Color.white;
        }
        proceedToTeamFailure();
    }

    /// <summary>
    /// showTurnResult() => Affiche un commentaire à la suite de la fin d'un jet de gobelet :
    ///     - Le message est écrit à l'écran
    ///     - Une animation est affichée avec un mouvement
    ///     - On affiche un panneau avec les scores
    /// </summary>
    /// <param name="msg">string msg => message à afficher</param>
    private void showTurnResult(string msg)
    {
        mTurnResult.DOKill();
        mTurnResult.text = msg;
        mTurnResult.gameObject.SetActive(true);
        mTurnResult.transform.DOMoveY(mTurnResult.transform.position.y + -100.0f, 1.0f).From().SetLink(mTurnResult.gameObject);
        showPanel();
        DOVirtual.DelayedCall(1.0f, () =>
        {
            mTurnResult.gameObject.SetActive(false);
        }).SetLink(mTurnResult.gameObject);
    }

    private void showPanel()
    {
        if(mMainMenuON == false)
            mPanel.transform.DOMoveY(mDownPanelPoint.transform.position.y, 1.0f).SetLink(mPanel);
    }

    private void hidePanel()
    {
        if(mMainMenuON == false)
            mPanel.transform.DOMoveY(mUpPanelPoint.transform.position.y , 1.0f).SetLink(mPanel);
    }

    private void updateTimer()
    {
        if (mTimer > 0)
        {
            mTimer--;
            mTimerText.text = mTimer.ToString();
        }
    }

    /// <summary>
    /// onRotationStart() => appelé depuis le gobelet quand la rotation de celui-ci commence
    /// </summary>
    public void onRotationStart()
    {
        hidePanel();
    }

    private void hideWinPanel()
    {
        if(mWinPanel != false)
            mWinPanel.SetActive(false);
    }

    private void showWinPanel()
    {
        mWinPanel.SetActive(true);
        mPanel.SetActive(false);
        mTurnResult.gameObject.SetActive(false);
    }

    private void hideFailPanel()
    {
        if(mFailPanel != null)
            mFailPanel.SetActive(false);
    }

    private void showFailPanel()
    {
        mFailPanel.SetActive(true);
        mPanel.SetActive(false);
        mTurnResult.gameObject.SetActive(false);
    }

    private void hideCotillons()
    {
        foreach(GameObject cotillon in mCotillonList)
        {
            cotillon.SetActive(false);
        }

    }

    private void showCotillons()
    {
        foreach(GameObject cotillon in mCotillonList)
        {
            cotillon.SetActive(true);
            cotillon.GetComponent<ParticleSystem>().Play();
        }
    }

}

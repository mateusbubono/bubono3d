﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GGVictoryGoblet : MonoBehaviour
{
    /// <summary>
    /// BBGoblet mGoblet => Référence vers le gobelet vers lequel le trigger va pouvoir notifier le succès de la rotation du Gobelet
    /// </summary>
    [SerializeField] BBGoblet mGoblet;

    /// <summary>
    /// OnTriggerEnter() => Le trigger de victoire notifie le succès de la rotation du gobelet en envoyant un message au Gobelet qui va gérer le succès du move
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        float rotationValue = Mathf.Abs(transform.rotation.z);
        if (mGoblet.mRotateON && rotationValue >= 0.0f && rotationValue <= 0.1f)
        {
            mGoblet.onRotateSuccess();
        }
    }

    /// <summary>
    /// OnTriggerExit() => si le trigger de victoire perd le contact avec la table lors de l'atterissage du gobelet il en notifie le Gobelet qui va invalider la victoire
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        mGoblet.onRotateNotStable();
    }
}

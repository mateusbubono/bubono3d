﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Hellmade.Sound;

public class BBGoblet : MonoBehaviour
{

    /// <summary>
    /// Vector3 up => Force à laquelle le gobelet est envoyé en l'air
    /// </summary>
    [SerializeField] Vector3 up = new Vector3(0.0f, 250.0f, 0.0f);

    /// <summary>
    ///  Vector3 rotate => Vecteur utilisé pour le mouvement de rotation du Gobelet
    /// </summary>
    [SerializeField] Vector3 rotate = new Vector3(0.0f, 0.0f, 1.5f);

    /// <summary>
    /// float mRotateSpeedMin => vitesse de rotation minimum lors de la projection du verre. La vitesse de rotation est une valeur aléatoire entre une vitesse min et max
    /// </summary>
    [SerializeField] float mRotateSpeedMin = 1.0f;

    /// <summary>
    /// float mRotateSpeedMax => vitesse de rotation maximum lors de la projection du verre. La vitesse de rotation est une valeur aléatoire entre une vitesse min et max
    /// </summary>
    [SerializeField] float mRotateSpeedMax = 20.0f;

    /// <summary>
    /// float mRotateSpeed => La vitesse de rotation calculée aléatoirement entre une borne mRotateSpeedMin et mRotateSpeedMax
    /// </summary>
    private float mRotateSpeed;

    /// <summary>
    /// GGPlayerController mPlayerController => Un lien vers le player controller qui permet au joueur d'agir sur le verre
    /// </summary>
    [SerializeField] GGPlayerController mPlayerController;

    /// <summary>
    /// Transform mGobletInitialPosition => poisition initiale du gobelet sur la table au début de chaque tour de jeu
    /// </summary>
    [SerializeField] Transform mGobletInitialPosition;

    [SerializeField] private AudioClip mThrowGobletSound;

    [SerializeField] private AudioClip mSuccessSound;


    [SerializeField] private AudioClip mFailureSound;

    /// <summary>
    ///  GGGameManager mGameManager => Référence vers le GameManager
    /// </summary>
    private GGGameManager mGameManager;


    /// <summary>
    /// bool mRotateON => Ce flag indique que le gobelet est projeté dans les airs et qu'il peut tourner
    /// </summary>
    public bool mRotateON { get; set; }

    /// <summary>
    /// bool mRotateMoveON => Ce flag actif fait aussi tourner le verre mais désactive le mouvement de rotation dès lors que le verre attérit sur la table
    /// </summary>
    public bool mRotateMoveON;

    /// <summary>
    /// bool mSuccessFlag => Le succès d'un lancé est mesuré par deux trigger indiquant que le verre est retombé sur sa base sans retomber sur le côté. Ce flag indique l'activation d'un premier trigger 
    /// </summary>
    private bool mSuccessFlag;

    // Start is called before the first frame update
    void Start()
    {
        mGameManager =  GameObject.FindGameObjectWithTag("GameManager").GetComponent<GGGameManager>();
        mSuccessFlag = false;
        mRotateMoveON = true;
    }

    /// <summary>
    /// FixedUpdate() => Gère la rotation du Gobelet qui est active :
    ///     - Si le flag mRotateON est actif => activé quand le joueur projète le gobelet en l'air et se désactive à la fin du tour du joueur
    ///     - Et si le flag mRotateMoveON est actif => il s'active au début du tour du joueur et se désactive lorsque le gobelet projeté touche la table
    /// </summary>
    private void FixedUpdate()
    {
        if (mRotateON && mRotateMoveON)
            transform.RotateAround(transform.position, rotate, mRotateSpeed * Time.deltaTime);
    }

    /// <summary>
    /// OnMouseDown() => Cette fonction gère le lancé du gobelet par le joueur qui va appuyer dessus
    /// </summary>
    private void OnMouseDown()
    {
        /*
        if(mGameManager.mGameState == GGGameParametes.eGameState.INGAME)
            mPlayerController.rotateGoblet();*/

    }

    /// <summary>
    /// OnCollisionEnter() => désactive la flag mRotateMoveON lorsque le gobelet retombe sur la table (inactive le mouvement de rotation)
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        if(mRotateON)
            mRotateMoveON = false;
    }

    /// <summary>
    /// rotateGoblet() => Agis sur le gobelet pour le propulser en l'air et le faire tourner. La vitesse de rotation est calculée aléatoirement entre un minimum et un maximum
    /// </summary>
    /// <param name="rotateSpeedMin">float rotateSpeedMin = -1.0f => vitesse de rotation minimum. Si la paramètre vaut -1.0f alors les réglage par défaut du gobelet sont choisi </param>
    /// <param name="rotateSpeedMax">float rotateSpeedMax = -1.0f => vitesse de rotation maximum. Si la paramètre vaut -1.0f alors les réglage par défaut du gobelet sont choisi</param>
    public void rotateGoblet( float rotateSpeedMin = -1.0f, float rotateSpeedMax= 1.0f )
    {
        mGameManager.onRotationStart();
        GetComponent<Rigidbody>().AddForce(up);
        mRotateON = true;

        if(rotateSpeedMin == -1.0f || rotateSpeedMax == -1.0f)
            mRotateSpeed = Random.Range(mRotateSpeedMin, mRotateSpeedMax);
        else
            mRotateSpeed = Random.Range(rotateSpeedMin, rotateSpeedMax);

        DOVirtual.DelayedCall(0.01f, () =>
        {
            EazySoundManager.PlaySound(mThrowGobletSound);
        });
    }

    /// <summary>
    /// onRotateSuccess() => Une rotation est un succès si le gobelet fait un tour sur lui même et retombe sur sa base. Pour que le succès soit actif la fonction doit être appelée deux fois par deux trigger différents
    ///     - La première fois un premier trigger est activé la flag mSuccessFlag est activé
    ///     - La seconde fois le second trigger est activé le flag mSuccessFlag étant déjà actif on lance la procédure de succès dans le GameManager
    ///     - Si le contact avec la table est perdu par l'un des deux trigger le flag mSuccessFlag redevient faux et la rotation devient invalide
    /// </summary>
    public void onRotateSuccess()
    {
        if (mSuccessFlag)
        {
            EazySoundManager.PlaySound(mSuccessSound);
            mGameManager.onRotateSuccess();
            restartGobletPosition();
            mRotateON = false;
        }
        else
        {
            mRotateMoveON = false;
            mSuccessFlag = true;
        }
    }

    /// <summary>
    /// onRotateNotStable() => Fonction appelée si le contact entre un trigger de succès et la table est perdu
    /// </summary>
    public void onRotateNotStable()
    {
        mSuccessFlag = false;
    }

    /// <summary>
    /// onRotateFail() => La fonction est appelée si le verre ne retombe pas sur sa base. Dans ce cas la procédure d'échec est lancé dans le GameManager et la position du verre est reset pour le tour suivant
    /// </summary>
    public void onRotateFail()
    {
        EazySoundManager.PlaySound(mFailureSound);

        mRotateON = false;
        mGameManager.onRotateFail();
        restartGobletPosition();
    }

    /// <summary>
    /// restartGobletPosition() => La fonction va réinitialiser la position du verre :
    /// </summary>
    private void restartGobletPosition()
    {
        transform.DOKill();
        DOVirtual.DelayedCall(0.5f, () =>
        {
            mSuccessFlag = false;
            mRotateMoveON = true;
            transform.position = mGobletInitialPosition.position;
            transform.rotation = mGobletInitialPosition.rotation;
            Invoke("onPlayerEndTurn", 0.5f);
        }).SetLink(gameObject);
    }

    /// <summary>
    /// onPlayerEndTurn() => Le joueur notifie la fin de son tour au GameManager qui va activer le tour du joueur suivant
    /// </summary>
    private void onPlayerEndTurn()
    {
        mGameManager.onPlayerEndTurn();
    }
}

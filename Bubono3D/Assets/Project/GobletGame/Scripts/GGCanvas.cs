﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GGCanvas : MonoBehaviour
{
    [SerializeField] GameObject[] mUpButtonList;
    [SerializeField] GameObject mGameTitle;
    [SerializeField] GameObject mOptionsPanel;

    [SerializeField] UnityEngine.UI.InputField mPlayerPseudo;

    GGGameManager mGameManager;

    public void play()
    {
        string playerPseudo = mPlayerPseudo.text == "" ? "Player" : mPlayerPseudo.text;
        PlayerPrefs.SetString("PlayerPseudo", playerPseudo);
        SceneManager.LoadScene("GG_MainScene");
    }

    public void nextLevel()
    {
        mGameManager.nextLevel();
    }

    public void restart()
    {
        mGameManager.restart();
    }

    public void home()
    {
        SceneManager.LoadScene("GG_MenuScene");
    }

    // Start is called before the first frame update
    void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GGGameManager>();

        if(mPlayerPseudo != null)
            mPlayerPseudo.text = PlayerPrefs.GetString("PlayerPseudo", "");

        if (mUpButtonList != null)
        {
            foreach(GameObject button in mUpButtonList)
            {
                button.transform.DOScale(1.2f, 1.0f).SetEase(Ease.OutBounce).SetLoops(-1, LoopType.Yoyo).SetLink(button);
            }
        }
            

        if(mGameTitle != null)
            mGameTitle.transform.DOScale(0.8f, 5.0f).SetLoops(-1, LoopType.Yoyo).SetLink(mGameTitle);
    }

    public void showOptions()
    {
        mOptionsPanel.SetActive(true);
    }
}

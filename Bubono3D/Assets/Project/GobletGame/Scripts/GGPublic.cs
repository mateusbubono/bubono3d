﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class GGPublic : MonoBehaviour
{

    [SerializeField] private AudioClip mWowSound;
    [SerializeField] private AudioClip mAahSound;
    [SerializeField] private AudioClip mEndingJingle;

    private bool mPublicVoiceON = true;

    /// <summary>
    /// int mTeamNumber => chaque public est attribué à une équipe qui soutient un joueur
    /// </summary>
    public int mTeamNumber = 0;

    /// <summary>
    /// List<Animator> publicTab => le public contient des personnages chacun équipé d'un gestionnaire d'animation, qui est récupéré dans cette liste
    /// </summary>
    private List<Animator> publicTab;

    /// <summary>
    /// Start() => La fonction va récupérer pour chaque personnage lié au GameObject public, l'objet Animator lié à ce personnage et le stocker dans une liste. De cette
    /// façon le GameObject public sera en mesure de déclencher les animations liés à chaque personnage appartenant à un même public 
    /// </summary>
    void Start()
    {
        publicTab = new List<Animator>();
        int childNb = transform.childCount;

        for(int i = 0; i<childNb; i++)
        {
            publicTab.Add(transform.GetChild(i).gameObject.GetComponent<Animator>());
        }

    }


    /// <summary>
    /// onTeamSuccess() => Lorsque l'équipe marque un point, ou que la team adverse échoue, une procédure de succès est levée et des animations de succès sont lancé pour chaque personnage du public
    /// </summary>
    public void onTeamSuccess(bool endGame=false)
    {
        foreach(Animator characterAnimator in publicTab)
        {
            float randomTime = Random.Range(0.0f, 0.75f);
            DOVirtual.DelayedCall(randomTime,
                ()=>
                {
                    if (mTeamNumber == 0 && mPublicVoiceON && endGame) // TODO => A ajuster
                    {
                        mPublicVoiceON = false;
                        EazySoundManager.PlaySound(mWowSound, 0.3f);
                        EazySoundManager.PlaySound(mEndingJingle, 1.0f);
                    }
                    characterAnimator.SetBool("CheerON", true);
                }
             );
            
        }

        Invoke("disableCheer", 2.0f);
    }

    /// <summary>
    /// disableCheer() => Permet de stopper l'animation d'acclamation des personnages du public
    /// </summary>
    private void disableCheer()
    {
        if (mPublicVoiceON == false)
        {
            mPublicVoiceON = true;

        }
        foreach (Animator characterAnimator in publicTab)
        {
            characterAnimator.SetBool("CheerON", false);
        }
    }

    /// <summary>
    /// onTeamFail() => Lorsque l'équipe rate un lancé, ou que la team adverse marque un point, une procédure d'échec est levée et des animations de déceptions sont lancés pour chaque personnage du public
    /// </summary>
    public void onTeamFail(bool endGame=false)
    {
        foreach (Animator characterAnimator in publicTab)
        {
            float randomTime = Random.Range(0.0f, 0.75f);
            DOVirtual.DelayedCall(randomTime,
                () =>
                {
                    if(mTeamNumber == 0 && mPublicVoiceON && endGame) //TODO à ajuster
                    {
                        EazySoundManager.PlaySound(mAahSound, 0.3f);
                        mPublicVoiceON = false;
                    }
                    characterAnimator.SetBool("DefeatON", true);
                }
           );
        }

        Invoke("disableDefeat", 2.0f);
    }

    /// <summary>
    /// disableDefeat() => Permet de stopper l'animation de déception des personnages du public
    /// </summary>
    private void disableDefeat()
    {
        if(mPublicVoiceON == false)
        {
            mPublicVoiceON = true;
        }
        foreach (Animator characterAnimator in publicTab)
        {
            characterAnimator.SetBool("DefeatON", false);
        }
    }
}

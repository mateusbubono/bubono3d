﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GGOptionsPanel : MonoBehaviour
{
    [SerializeField] Sprite mMusicImageON;
    [SerializeField] Sprite mMusicImageOFF;

    [SerializeField] Sprite mPhoneShakeON;
    [SerializeField] Sprite mPhoneShakeOFF;

    [SerializeField] Image mMusicImage;
    [SerializeField] Image mPhoneShake;

    private void OnEnable()
    {
        int musicON = PlayerPrefs.GetInt("MusicON", 1);

        if(musicON == 1)
        {
            mMusicImage.sprite = mMusicImageON;
        }
        else
        {
            mMusicImage.sprite = mMusicImageOFF;
        }

        int phoneShakeON = PlayerPrefs.GetInt("PhoneShakeON", 1);
        if(phoneShakeON == 1)
        {
            mPhoneShake.sprite = mPhoneShakeON;
        }
        else
        {
            mPhoneShake.sprite = mPhoneShakeOFF;
        }
    }

    public void hideOptionsPanel()
    {
        gameObject.SetActive(false);
    }

    public void switchMusic()
    {
        if(mMusicImage.sprite == mMusicImageON)
        {
            mMusicImage.sprite = mMusicImageOFF;
            PlayerPrefs.SetInt("MusicON", 0);
        }
        else
        {
            mMusicImage.sprite = mMusicImageON;
            PlayerPrefs.SetInt("MusicON", 1);
        }
    }

    public void switchPhoneShake()
    {
        if (mPhoneShake.sprite == mPhoneShakeON)
        {
            mPhoneShake.sprite = mPhoneShakeOFF;
            PlayerPrefs.SetInt("PhoneShakeON", 0);
        }
        else
        {
            mPhoneShake.sprite = mPhoneShakeON;
            PlayerPrefs.SetInt("PhoneShakeON", 1);
        }
    }
}

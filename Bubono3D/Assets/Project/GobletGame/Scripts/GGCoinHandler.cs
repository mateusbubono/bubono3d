﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GGCoinHandler : MonoBehaviour
{
    [SerializeField] Text mCoinBank;
    [SerializeField] Text mCoinEarn;

    [SerializeField] int mCoinEarnMin;
    [SerializeField] int mCoinEarnMax;

    private void showCoinEarned()
    {
        int coinEarned = Mathf.RoundToInt(Random.Range(mCoinEarnMin, mCoinEarnMax));

        int coinBank = PlayerPrefs.GetInt("CoinBank", 0);
        mCoinBank.text = coinBank.ToString();
        mCoinEarn.text = "+" + coinEarned.ToString();

        DOVirtual.DelayedCall(1.0f, () =>
        {
            coinBank += coinEarned;
            mCoinBank.text = coinBank.ToString();
            mCoinEarn.gameObject.SetActive(false);
            PlayerPrefs.SetInt("CoinBank", coinBank);
        }).SetLink(gameObject);
    }

    private void OnEnable()
    {
        showCoinEarned();
    }


}

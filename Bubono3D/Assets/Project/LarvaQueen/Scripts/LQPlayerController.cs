﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LQPlayerController : MonoBehaviour
{
    [SerializeField] LQLarvaQueen mLarvaQueen;
    [SerializeField] float mMouseSensitivity = 3.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;

        spawnLarva(Input.GetMouseButton(0), mLarvaQueen.isLarvaSpawnON());
        rotateLarvaQueen();
    }

    void rotateLarvaQueen()
    {
        float rotationAmountX = Input.GetAxis("Mouse X") * mMouseSensitivity;
        float rotationAmountY = Input.GetAxis("Mouse Y") * mMouseSensitivity;

        Vector3 rotationLarvaQueen = mLarvaQueen.transform.rotation.eulerAngles;

        rotationLarvaQueen.y += rotationAmountX;
        rotationLarvaQueen.z += 0.0f;
        rotationLarvaQueen.x -= rotationAmountY;

        mLarvaQueen.transform.rotation = Quaternion.Euler(rotationLarvaQueen);
    }

    void spawnLarva(bool isMouseButtonDown, bool isLarvaSpawnON)
    {
        if (isMouseButtonDown && isLarvaSpawnON == false)
        {
            mLarvaQueen.enableLarvaSpawn();
        }
        else if (!isMouseButtonDown && isLarvaSpawnON == true)
        {
            mLarvaQueen.disableLarvaSpawn();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LQLarvaQueen : MonoBehaviour
{
    [SerializeField] GameObject mLarva;
    [SerializeField] GameObject mLarvaSpawnPoint;
    [SerializeField] float mSpawnForceMultiplier = 750.0f;

    [SerializeField] bool mLarvaSpawnON = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("spawnLarva", 0.3f, 0.3f);
    }

    private void spawnLarva()
    {
        if(mLarvaSpawnON)
        {
            GameObject larva = Instantiate(mLarva, mLarvaSpawnPoint.transform.position, mLarvaSpawnPoint.transform.rotation);
            larva.GetComponent<Rigidbody>().AddForce(transform.forward * mSpawnForceMultiplier);
        }
    }

    public void enableLarvaSpawn()
    {
        mLarvaSpawnON = true;
    }

    public void disableLarvaSpawn()
    {
        mLarvaSpawnON = false;
    }

    public bool isLarvaSpawnON()
    {
        return mLarvaSpawnON;
    }
}

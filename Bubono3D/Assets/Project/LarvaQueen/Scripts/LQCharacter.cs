﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LQCharacter : MonoBehaviour
{
    [SerializeField] float mCharacterLife = 0.0f;
    [SerializeField] float mCharacterDamageAmount = 0.01f;
    [SerializeField] float mNumberOfLarva = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        takeDamage();
    }

    void takeDamage()
    {
        if (transform.localScale.x <= 0.0f)
        {
            Destroy(gameObject);
        }
        if (mCharacterLife <= 0.0f)
        {
            transform.localScale = transform.localScale - new Vector3(mCharacterDamageAmount, mCharacterDamageAmount, mCharacterDamageAmount) * mNumberOfLarva;
        }
        else
        {
            mCharacterLife -= mNumberOfLarva * mCharacterDamageAmount;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.GetComponent<FixedJoint>().connectedBody = GetComponent<Rigidbody>();
        
        mNumberOfLarva += 1.0f;
    }

    /*private void OnCollisionExit(Collision collision)
    {
        if(mNumberOfLarva >= 0.0f)
            mNumberOfLarva -= 1.0f;
    }*/
}

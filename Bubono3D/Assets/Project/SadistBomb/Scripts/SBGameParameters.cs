﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SBGameParameters : MonoBehaviour
{

    public enum eSBGameState
    {
        WARMUP,
        START,
        INGAME,
        GAMEOVER
    }
}

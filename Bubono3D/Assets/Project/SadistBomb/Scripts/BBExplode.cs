﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBExplode : MonoBehaviour
{
    /// <summary>
    /// SBGameManager mGameManager => Référence vers le GameManager
    /// </summary>
    SBGameManager mGameManager;

    /// <summary>
    /// ParticleSystem mExplodeParticleSystem => Particle effect du déclenchement de l'explosion
    /// </summary>
    [SerializeField] ParticleSystem mExplodeParticleSystem;

    /// <summary>
    /// Awake() => Récupère une référence vers le GameManager et désactive le particle effect de l'explosion
    /// </summary>
    private void Awake()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SBGameManager>();
        mExplodeParticleSystem.gameObject.SetActive(false);
    }

    /// <summary>
    /// OnTriggerEnter() => Lorsqu'une victime entre dans le champs de l'explosion celle-ci est détruite. La destruction d'une victime est notifiée dans le GameManager
    /// </summary>
    /// <param name="other">Collider other => objet de collision</param>
    private void OnTriggerEnter(Collider other)
    {
         switch(other.gameObject.tag)
         {
             case "SB_GoodVictim":
                mGameManager.onVictimDestroyed(100);
                other.gameObject.GetComponent<BBVictim>().selfDestroy();
                break;
             case "SB_BadVictim":
                mGameManager.onVictimDestroyed(200);
                other.gameObject.GetComponent<BBVictim>().selfDestroy();
                break;
         }
    }

    private void OnEnable()
    {
        mExplodeParticleSystem.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        mExplodeParticleSystem.gameObject.SetActive(false);
    }
}



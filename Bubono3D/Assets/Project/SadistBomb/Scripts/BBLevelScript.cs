﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBLevelScript : MonoBehaviour
{
    /// <summary>
    /// float mRandomSpawnRangeMin => Réglage du spawn des victime (0.0f == mauvaise victime >0.0f == bonne victime
    /// </summary>
    public float mRandomSpawnRangeMin = 0.0f;

    /// <summary>
    ///  float mRandomSpawnRangeMax => Réglage du Spawn des victime plus la valeur max est élevée plus les bonne victimes seront majoritaire par rapport aux mauvaises victimes
    /// </summary>
    public float mRandomSpawnRangeMax = 3.0f;

    /// <summary>
    /// float mGoodVictimSpeed => la vitesse de déplacement des victimes en état promenade (déplacement géré à partir du NavMeshAgent)
    /// </summary>
    public float mGoodVictimSpeed = 3.5f;

    /// <summary>
    /// float mGoodVictimFleeSpeed => La vitesse de déplacement des victimes en mode fuite (déplacement gérée par la victime)
    /// </summary>
    public float mGoodVictimFleeSpeed = 3.5f;

    /// <summary>
    /// float mBadVictimSpeed =>  la vitesse de déplacement des victimes en état promenade (déplacement géré à partir du NavMeshAgent)
    /// </summary>
    public float mBadVictimSpeed = 3.5f;

    /// <summary>
    /// float mBadVictimChaseSpeed =>  La vitesse de déplacement des victimes en mode chasse (déplacement gérée par la victime)
    /// </summary>
    public float mBadVictimChaseSpeed = 3.5f;

    /// <summary>
    /// int mNumberOfVictim => nombre de victime qui vont être spawn par point de spawn
    /// </summary>
    public int mNumberOfVictim = 3;

}

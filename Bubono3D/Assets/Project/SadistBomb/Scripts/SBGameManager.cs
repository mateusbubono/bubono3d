﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;
using DG.Tweening;

public class SBGameManager : MonoBehaviour
{
    /// <summary>
    /// SBGameParameters.eSBGameState mGameState => Variable représentant l'état de la partie 
    /// </summary>
    public SBGameParameters.eSBGameState mGameState = SBGameParameters.eSBGameState.WARMUP;

    /// <summary>
    /// int mScore => le score du joueur
    /// </summary>
    private int mScore;

    /// <summary>
    /// int mVictimTotalNb => le nombre total de victime présente dans le niveau
    /// </summary>
    private int mVictimTotalNb;

    /// <summary>
    /// int mVictimNb => Le nombre de victime restante
    /// </summary>
    private int mVictimNb;

    /// <summary>
    /// Text mScoreText => UI du score du joueur
    /// </summary>
    [SerializeField] private Text mScoreText;

    /// <summary>
    ///  Text mVictimNbText => UI du nombre de victime en jeu
    /// </summary>
    [SerializeField] private Text mVictimNbText;

    /// <summary>
    /// GameObject[] mLevels => table de l'ensemble des niveaux
    /// </summary>
    [SerializeField] GameObject[] mLevels;

    private GameObject mLevel;

    /// <summary>
    /// int mCurrentLevel => niveau qui doit être chargé (-1 et le niveau chargé sera le niveau sauvé. Sinon le niveau chargé sera le niveau indiqué dans la variable
    /// </summary>
    [SerializeField] int mCurrentLevel = -1;

    /// <summary>
    /// GameObject[] mPlanes => Table des plateforme utilisée pour faire un Build du NavMesh du niveau
    /// </summary>
    GameObject[] mPlanes;

    /// <summary>
    /// GameObject mEndCanvas => Ecran de fin de partie
    /// </summary>
    [SerializeField]  GameObject mEndCanvas;

    /// <summary>
    /// GameObject mStartCanvas => Ecran de début de partie
    /// </summary>
    [SerializeField]  GameObject mStartCanvas;

    /// <summary>
    ///  GameObject mWinPanel => Panel de victoire qui s'affiche dans l'écran de fin en cas de victoire
    /// </summary>
    [SerializeField]  GameObject mWinPanel;

    /// <summary>
    /// GameObject mFailPanel => Panel de défaite qui s'affiche dans l'écran de fin en cas de défaite
    /// </summary>
    [SerializeField]  GameObject mFailPanel;

    /// <summary>
    ///  ParticleSystem mVictoryEffect => Effet de victoire 
    /// </summary>
    [SerializeField] ParticleSystem mVictoryEffect;

    /// <summary>
    /// ParticleSystem mDefeatEffect => Effet de défaite
    /// </summary>
    [SerializeField] ParticleSystem mDefeatEffect;

    private void Awake()
    {
        loadLevel();
    }

    /// <summary>
    /// Start() => Initialisation de la partie :
    ///     - Désactivation des panels et des écrans de fin et de départ
    ///     - Désactivation des effet de victoire et de défaite
    ///     - Activation des NavMesh
    ///     - Initialisation du score
    ///     - Lancement de l'ecoUpdate uniquement en mode éditeur
    /// </summary>
    void Start()
    {

        mWinPanel.SetActive(false);
        mFailPanel.SetActive(false);
        mEndCanvas.SetActive(false);
        mStartCanvas.SetActive(false);
        mVictoryEffect.gameObject.SetActive(false);
        mDefeatEffect.gameObject.SetActive(false);

        mVictimTotalNb = 0;

        mPlanes = GameObject.FindGameObjectsWithTag("SB_Plane");
        foreach(GameObject plane in mPlanes)
        {
            plane.GetComponent<NavMeshSurface>().BuildNavMesh();
        }

        mScore = 0;
        mScoreText.text = mScore.ToString();

        InvokeRepeating("ecoUpdate", 0.1f, 0.1f);
    }

    /// <summary>
    /// loadLevel() => Chargement du niveau :
    ///     - Les niveaux sont sauvés dans une PlayerPrefs. Le niveau est récupéré en fonction de la valeur de mCurrentLevel
    ///     - Le niveau choisie est instantié 
    ///     - s'en suit une petite animation et le lancement de l'instanciation des victime
    ///     - Le GameState est ensuite changé de WarmUp à Start à la suite d'un petit délai
    /// </summary>
    void loadLevel()
    {
        if(mCurrentLevel == -1)
        {
            mCurrentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        }
        mLevel = Instantiate(mLevels[mCurrentLevel]);
        mLevel.transform.parent = transform;

        mLevel.transform.DOMoveY(-100.0f, 1.0f).From().SetLink(mLevel);
        
    }

    /// <summary>
    /// switchGameToStart() => changement du GameState à Start et activation de l'écran de départ
    /// </summary>
    void switchGameToStart()
    {
        mStartCanvas.SetActive(true);
        mGameState = SBGameParameters.eSBGameState.START;
    }


    public void ecoUpdate()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
        {
            restart();
        }
        #endif

        if(mLevel.transform.position.y == 0.0f)
        {
            BroadcastMessage("onGameStart");
            Invoke("switchGameToStart", 0.3f);
            CancelInvoke("ecoUpdate");
        }

    }

    /// <summary>
    /// onVictimDestroyed() => Lorsqu'une victime est détruite le GameManager est notifié le nombre de victime est mis à jour et le score est ajusté
    /// </summary>
    /// <param name="score">int score => le score mis à ajouter</param>
    public void onVictimDestroyed(int score)
    {
        updateScore(score);
        decreaseVictimNb();
    }

    /// <summary>
    /// onBombDestroyed() => Lorsque la bombe est détruite le GameManager est notifié et la fonction de fail est appelée
    /// </summary>
    public void onBombDestroyed()
    {
        fail();
    }

    /// <summary>
    /// updateScore() => Met à jour le score
    /// </summary>
    /// <param name="score">int score => score à ajouter au score global du joueur</param>
    private void updateScore(int score)
    {
        mScore += score;
        mScoreText.text = mScore.ToString();
    }

    /// <summary>
    /// decreaseVictimNb() => décrémente le nombre de victime à chaque destruction de victime
    /// </summary>
    private void decreaseVictimNb()
    {
        mVictimNb--;
        mVictimNbText.text = mVictimNb + "/" + mVictimTotalNb;

        if (mVictimNb <= 0)
            win();
    }

    /// <summary>
    /// restart() => recharge de la scène
    /// </summary>
    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// nextLevel() => la fonction va calculer le prochain niveau et recharger la scène et charger le niveau
    /// </summary>
    public void nextLevel()
    {
        mCurrentLevel = (mCurrentLevel + 1) % mLevels.Length;
        PlayerPrefs.SetInt("currentLevel", mCurrentLevel);
        restart();
    }

    /// <summary>
    /// gameOver() => fonction appelée à la fin d'une partie
    /// </summary>
    private void gameOver()
    {
        mGameState = SBGameParameters.eSBGameState.GAMEOVER;
        mEndCanvas.SetActive(true);
    }

    /// <summary>
    /// fail() => fonction appelée quand le joueur à perdu la partie
    /// </summary>
    private void fail()
    {
        gameOver();
        mDefeatEffect.gameObject.SetActive(true);
        DOVirtual.DelayedCall(0.5f, () =>
        {
            mFailPanel.SetActive(true);
        });
    }

    /// <summary>
    /// win() => fonction appelée quand le joueur à gagné la partie
    /// </summary>
    private void win()
    {
        gameOver();

        mVictoryEffect.gameObject.SetActive(true);

        DOVirtual.DelayedCall(0.5f, () =>
        {
            mWinPanel.SetActive(true);
        });
        
    }

    /// <summary>
    /// onStart() => La fonction est appelée lorsque le joueur tape la première fois sur l'écran et que l'écran de début de partie est activé. Il passe alors le GameState à INGAME
    /// </summary>
    public void onStart()
    {
        mStartCanvas.SetActive(false);
        mGameState = SBGameParameters.eSBGameState.INGAME;
        //updateVictimNb();

        //L'instantiation des victimes est en asynchrone aussi on rappelle  updateVictimNb car le premier comptage des victimes n'est pas très précis
        //Invoke("updateVictimNb", 0.5f);

    }

    /// <summary>
    /// updateVictimNb() => Cette fonction calcule le nombre de victime présente dans un niveau de jeu
    /// </summary>
    private void updateVictimNb()
    {
        mVictimTotalNb = GameObject.FindGameObjectsWithTag("SB_GoodVictim").Length + GameObject.FindGameObjectsWithTag("SB_BadVictim").Length;
        mVictimNb = mVictimTotalNb;
        mVictimNbText.text = mVictimNb + "/" + mVictimTotalNb;
    }

    public void increaseVictimNb(int victimNb)
    {
        mVictimTotalNb += victimNb;
        mVictimNb = mVictimTotalNb;
        mVictimNbText.text = mVictimNb + "/" + mVictimTotalNb;
    }

}

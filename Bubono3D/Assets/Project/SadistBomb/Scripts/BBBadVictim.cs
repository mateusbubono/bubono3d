﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBBadVictim : BBVictim
{
    /// <summary>
    /// Vector3 mChaseDirection : Vecteur indiquant la direction de la bombe que la mauvaise victime doit chasser
    /// </summary>
    private Vector3 mChaseDirection;

    new void Start()
    {
        base.Start();
    }

    /// <summary>
    ///  Update() => Appelle a chaque frame une fonction de comportement de la victime en fonction de sont état :
    ///     - En attente
    ///     - En promenade
    ///     - En chasse
    /// </summary>
    protected override void Update()
    {
        switch (mVictimState)
        {
            case eVictimState.WAIT:
                wait();
                break;
            case eVictimState.WALK:
                walk();
                break;
            case eVictimState.CHASE:
                chase();
                break;
        }

    }


    /// <summary>
    /// onBombDetectionStart() => Fonction appelée lorsque la bombe est détectée par le champs de détection de la victime. Elle va alors changer le comportement de la victime pour le passer en chasse
    /// </summary>
    /// <param name="bombPosition">Vector3 bombPosition : la position de la bombe que la victime va devoir traquée</param>
    override public void onBombDetectionStart(Vector3 bombPosition)
    {
        if (mVictimState != eVictimState.CHASE)
        {
            mVictimState = eVictimState.CHASE;

            mNavMeshAgent.speed = mLevelScript.mBadVictimChaseSpeed;
            mNavMeshAgent.SetDestination(bombPosition *1.5f - transform.position);

            mChaseDirection = bombPosition - transform.position;
        }
    }

    /// <summary>
    /// onBombDetectionStop() => Fonction appelée quand la bombe sort du champs de détection de la victime. Cela va changer le comportement de la victime et le passer en mode promenade
    /// </summary>
    override public void onBombDetectionStop()
    {
        if (mVictimState == eVictimState.CHASE)
        {
            mVictimState = eVictimState.WALK;
            mNavMeshAgent.speed = mLevelScript.mBadVictimSpeed;
        }
    }

    /// <summary>
    /// chase() => Fonction appelée afin que la victime traque la bombe
    /// </summary>
    private void chase()
    {
        mRigidbody.AddForce(mChaseDirection * mLevelScript.mBadVictimChaseSpeed * mDeltaTime);
    }
}

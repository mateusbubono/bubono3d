﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class BBGoodVictim : BBVictim
{

    /// <summary>
    /// Vector3 mFleeDirection => Direction choisie par une victime pour fuir la bombe détectée
    /// </summary>
    Vector3 mFleeDirection;

    new void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Update() => A chaque frame la victime va appliquer un comportement qui va dépendre de son état 
    ///     - Soit la victime va attendre
    ///     - Soit la victime va marcher
    ///     - Soit la victime va fuir la bombe
    /// </summary>
    protected override void Update()
    {
        switch(mVictimState)
        {
            case eVictimState.WAIT :
                wait();
                break;
            case eVictimState.WALK:
                walk();
                break;
            case eVictimState.FLEE:
                flee();
                break;
        }

    }

    /// <summary>
    /// flee() => Fonction appliquée pour que la victime puisse fuir la bombe qui est dans son champs de détection
    /// </summary>
    private void flee()
    {
        mRigidbody.AddForce(mFleeDirection * mLevelScript.mGoodVictimFleeSpeed * mDeltaTime);
    }

    /// <summary>
    /// onBombDetectionStart() => Lorsque le champs de détection détecte la bombe elle change l'état de la victime pour qu'elle passe en mode fuite
    /// </summary>
    /// <param name="bombPosition">Vector3 bombPosition => position de la bombe à fuir</param>
    override public void onBombDetectionStart(Vector3 bombPosition)
    {
        if (mVictimState != eVictimState.FLEE)
        {

            transform.DOScaleY(2.0f, 0.2f).SetLoops(2, LoopType.Yoyo).SetLink(gameObject);

            mExclamation.SetActive(true);

            mVictimState = eVictimState.FLEE;

            mNavMeshAgent.speed = mLevelScript.mGoodVictimFleeSpeed;
            mNavMeshAgent.SetDestination(bombPosition + transform.position);

            mFleeDirection = bombPosition + transform.position;
            mFleeDirection.Normalize();
        }
    }

    /// <summary>
    /// onBombDetectionStop() => Lorsque la bombe sort du champs de détection de la victime celle-ci passe de l'état fuite à l'état en promenade
    /// </summary>
    override public void onBombDetectionStop()
    {
        if (mVictimState == eVictimState.FLEE)
        {
            mExclamation.SetActive(false);
            mVictimState = eVictimState.WALK;
            mNavMeshAgent.speed = mLevelScript.mGoodVictimSpeed;
        }
    }
}

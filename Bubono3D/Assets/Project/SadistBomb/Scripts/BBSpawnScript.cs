﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BBSpawnScript : MonoBehaviour
{
    /// <summary>
    /// BBGoodVictim mGoodVictimPrefab => modèle de la bonne victime à instantier
    /// </summary>
    [SerializeField] BBGoodVictim mGoodVictimPrefab;

    /// <summary>
    /// BBBadVictim mBadVictimPrefab => modèle de la mauvaise victime à instantier
    /// </summary>
    [SerializeField] BBBadVictim mBadVictimPrefab;

    /// <summary>
    /// bool mSpawnScriptON => Si ce flag est désactiver le script n'instanciera pas de victime
    /// </summary>
    [SerializeField] private bool mSpawnScriptON = true;

    /// <summary>
    /// BBLevelScript mLevelScript => Référence vers le GameObject représentant le niveau
    /// </summary>
    BBLevelScript mLevelScript;

    SBGameManager mGameManager;

    
    void onGameStart()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SBGameManager>();
        if (mSpawnScriptON == false)
        {
            enabled = false;
            return;
        }

        Invoke("createVictim", 0.5f);
       
    }

    /// <summary>
    /// createVictim() => Le script de spawn va :
    ///     - Récupérer la référence de l'objet LevelScript qui contrôle le niveau du jeu
    ///     - Choisir aléatoirement de spawn une bonne ou une mauvaise victime en fonction d'un range minimum et maximum
    ///     - Spawn la victime choisie un certain nombre de fois (dépend du paramétrage du level script)
    /// </summary>
    private void createVictim()
    {
        mLevelScript = GameObject.FindGameObjectWithTag("SB_LevelScript").GetComponent<BBLevelScript>();
        int randomValue = Mathf.RoundToInt(Random.Range(mLevelScript.mRandomSpawnRangeMin, mLevelScript.mRandomSpawnRangeMax));

        BBVictim victimToSpawn = null;
        if (randomValue == 0)
            victimToSpawn = mBadVictimPrefab;
        else
            victimToSpawn = mGoodVictimPrefab;

        for (int i = 0; i < mLevelScript.mNumberOfVictim; i++)
            Instantiate(victimToSpawn, gameObject.transform.position, gameObject.transform.rotation);

        mGameManager.increaseVictimNb(mLevelScript.mNumberOfVictim);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBPlayerController : MonoBehaviour
{
    /// <summary>
    /// Joystick mJoystick => Référence vers le joystick virtuel qui va manipuler la bombe
    /// </summary>
    [SerializeField] Joystick mJoystick;

    /// <summary>
    /// BBBomb mBomb => référence vers la bombe contrôlée par le player controller
    /// </summary>
    [SerializeField] BBBomb mBomb;

    /// <summary>
    /// SBGameManager mGameManager => référence vers le GameManager
    /// </summary>
    SBGameManager mGameManager;

    void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SBGameManager>();
    }


    /// <summary>
    /// Update() => A chaque frame le player controller va tester s'il doit bouger la bombe et ou la faire exploser
    /// </summary>
    void Update()
    {
        if (mGameManager.mGameState == SBGameParameters.eSBGameState.START)
        {
            if (Input.GetMouseButtonDown(0))
                mGameManager.onStart();
            return;
        }
        else if(mGameManager.mGameState == SBGameParameters.eSBGameState.INGAME)
        {
            move();
            explode();
        }
        
    }

    /// <summary>
    /// move() => fonction qui gère le mouvement de la bombe
    /// </summary>
    private void move()
    {
        Vector3 direction = new Vector3(mJoystick.Horizontal, 0, mJoystick.Vertical).normalized;

        mBomb.move(direction);
    }

    /// <summary>
    /// explode() => fonction qui gère l'explosion de la bombe
    /// </summary>
    private void explode()
    {
        if (Input.GetMouseButtonUp(0))
        {
            mBomb.explode();
        }
    }
}

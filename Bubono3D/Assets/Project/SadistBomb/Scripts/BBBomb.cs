﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BBBomb : MonoBehaviour
{
    /// <summary>
    /// GameObject mNoRotationChild => Ce GameObject contient l'ensemble des sous objets de la bombe qui ne doivent pas tourner avec celle-ci
    /// </summary>
    [SerializeField] GameObject mNoRotationChild;

    /// <summary>
    /// GameObject mExplode => GameObject de l'explosion qui peut être déclenchée par la bombe
    /// </summary>
    [SerializeField] GameObject mExplode;

    /// <summary>
    /// GameObject mSphereScope => GameObject du scope de l'explosion de la bombe
    /// </summary>
    [SerializeField] GameObject mSphereScope;

    /// <summary>
    /// GameObject mIcon => GameObject de l'icone indiquant la position de la bombe
    /// </summary>
    [SerializeField] GameObject mIcon;

    /// <summary>
    /// float mSpeed => Vitesse de déplacement de la bombe
    /// </summary>
    [SerializeField] float mSpeed = 65.0f;

    /// <summary>
    /// ParticleSystem mExplodeReadyParticleSystem => Effet indiquant que l'explosion est prête de nouveau
    /// </summary>
    [SerializeField] ParticleSystem mExplodeReadyParticleSystem;

    /// <summary>
    /// float mExplodeCoolDownDelay => Délai en seconde de cooldown de l'explosion
    /// </summary>
    private float mExplodeCoolDownDelay = 3.0f;

    /// <summary>
    /// float mExplodeCoolDown => valeur mise à jour du CoolDown de l'explosion
    /// </summary>
    private float mExplodeCoolDown;

    /// <summary>
    /// bool mExplodeCoolDownON => Flag indiquant que le cooldown de l'explosion est activé
    /// </summary>
    private bool mExplodeCoolDownON;

    /// <summary>
    /// UnityEngine.UI.Text mExplodeCoolDownText => UI Text indiquant la durée du cooldown de l'explosion
    /// </summary>
    [SerializeField] UnityEngine.UI.Text mExplodeCoolDownText;

    /// <summary>
    /// SBGameManager mGameManager => Objet représentant l'instance du GameManager présent dans la partie
    /// </summary>
    private SBGameManager mGameManager;

    /// <summary>
    /// Rigidbody mRigidbody => rigidbody attaché à la bombe
    /// </summary>
    protected Rigidbody mRigidbody;

    /// <summary>
    /// GameObject mCamera => GameObject de la caméra
    /// </summary>
    private GameObject mCamera;

    /// <summary>
    /// float mDeltaTime => variable du deltatime 
    /// </summary>
    private float mDeltaTime;

    /// <summary>
    ///  Awake() => Initialisation de la bombe 
    ///     - Récupération du rigidbody
    ///     - Désactivation de l'explosion
    ///     - Désactivation du particle system de l'explosion
    ///     - Récupération du GameManager
    ///     - Initialisation des paramètres de cooldown de l'explosion
    /// </summary>
    private void Awake()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mExplode.SetActive(false);

        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SBGameManager>();


        mExplodeCoolDownON = false;
        mExplodeCoolDown = mExplodeCoolDownDelay;
        mExplodeCoolDownText.gameObject.SetActive(false);

        mExplodeReadyParticleSystem.gameObject.SetActive(false);

        mCamera = GameObject.FindGameObjectWithTag("MainCamera");

        mDeltaTime = Time.deltaTime;

    }

    void Update()
    {
        mNoRotationChild.transform.rotation = Quaternion.identity;

        if (mGameManager.mGameState == SBGameParameters.eSBGameState.GAMEOVER)
        {
            disableBomb();
            return;
        }

        updateCoolDown();
    }

    public void move(Vector3 direction)
    {
        mRigidbody.AddForce(direction * Time.deltaTime * mSpeed);
    }

    /// <summary>
    ///  explode() => Active l'explosion de la bombe
    ///     - Active l'effet d'explosion
    ///     - Activation du cooldown de l'explosion
    /// </summary>
    public void explode()
    {
        if(mExplodeCoolDownON == false)
        {
            mExplode.SetActive(true);
            mExplode.transform.DOScale(0.1f, 0.3f).From().SetLink(mExplode);
            mCamera.transform.DOShakePosition(0.5f, 1.0f);
            DOVirtual.DelayedCall(1.0f, () =>
            {
                mExplode.SetActive(false);
            }).SetLink(gameObject);

            mExplodeCoolDownText.gameObject.SetActive(true);
            mExplodeCoolDownON = true;
        }
    }

    /// <summary>
    /// OnCollisionEnter() => Gestion des collisions entrantes entre la bombes et les GameObjects :
    ///     - Collision avec les Victimes
    ///     - Collision avec la limite en cas de chute
    /// </summary>
    /// <param name="collision">Collision collision : Objet de collision</param>
    private void OnCollisionEnter(Collision collision)
    {
        switch(collision.gameObject.tag)
        {
            case "SB_GoodVictim":
                mGameManager.onVictimDestroyed(100);
                collision.gameObject.GetComponent<BBVictim>().selfDestroy();
                mCamera.transform.DOShakePosition(0.2f, 0.2f);
                bombGrowth();
                break;
            case "SB_BadVictim":
            case "SB_FallLimit":
                disableBomb();
                mGameManager.onBombDestroyed();
                break;
        }
    }

    /// <summary>
    /// disableBomb() => Désactivation de la bombe en cas de défaite ou fin de partie
    /// </summary>
    private void disableBomb()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        mExplode.SetActive(false);
        mSphereScope.SetActive(false);
        mIcon.SetActive(false);
    }

    /// <summary>
    /// updateCoolDown() => Fonction de mise à jour du cooldown de l'explosion. Lorsque le cooldown arrice à échéance un effet de particule signale que celle-ci est de nouveau active
    /// </summary>
    private void updateCoolDown()
    {
        if(mExplodeCoolDownON)
        {
            if(mExplodeCoolDown > 0)
            {
                mExplodeCoolDown -= Time.deltaTime;
                mExplodeCoolDownText.text = Mathf.RoundToInt(mExplodeCoolDown).ToString();
            }
            else
            {
                mExplodeCoolDownON = false;
                mExplodeCoolDown = mExplodeCoolDownDelay;
                mExplodeCoolDownText.gameObject.SetActive(false);
                mExplodeReadyParticleSystem.gameObject.SetActive(true);
                mExplodeReadyParticleSystem.Play();


                DOVirtual.DelayedCall(0.5f, () =>
                {
                    mExplodeReadyParticleSystem.gameObject.SetActive(true);
                }).SetLink(gameObject);
            }
            
        }
    }

    /// <summary>
    /// bombGrowth() => Gère le grossissement de la bombe à chaque victime écrasée
    /// </summary>
    private void bombGrowth()
    {
        Vector3 growthAdd = new Vector3(0.01f, 0.01f, 0.01f);
        transform.localScale += growthAdd;
        mNoRotationChild.transform.localScale -= new Vector3(0.0025f, 0.0025f, 0.0025f); ;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SBEndCanvas : MonoBehaviour
{
    /// <summary>
    /// SBGameManager mGameManager => Référence vers le GameManager
    /// </summary>
    SBGameManager mGameManager;

    // Start is called before the first frame update
    void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SBGameManager>();
    }

    public void home()
    {
        SceneManager.LoadScene("HomeScene");
    }

    public void nextLevel()
    {
        mGameManager.nextLevel();
    }

    public void restartLevel()
    {
        mGameManager.restart();
    }
}

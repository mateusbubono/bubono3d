﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SBHomeCanvas : MonoBehaviour
{
    private void Start()
    {
        int firstGame = PlayerPrefs.GetInt("FirstGame", 0);
        if(firstGame == 0)
        {
            SceneManager.LoadScene("MainScene");
            PlayerPrefs.SetInt("FirstGame", 1);
        }
    }

    public void runMainScene()
    {
        SceneManager.LoadScene("MainScene");
    }

}

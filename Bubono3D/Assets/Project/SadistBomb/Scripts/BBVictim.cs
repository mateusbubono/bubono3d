﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BBVictim : MonoBehaviour
{
    /// <summary>
    /// enum eVictimState => énumération qui représente les différents états que peuvent prendre les victimes :
    ///     - WAIT : victime en attente
    ///     - WALK : La victime se promène
    ///     - FLEE : La victime fuit la bombe qui la cible
    ///     - CHASE : La victime prend en chasse la bombe qui la cible
    /// </summary>
    protected enum eVictimState
    {
        WAIT,
        WALK,
        FLEE,
        CHASE
    }

    /// <summary>
    /// ParticleSystem mExplodeParticleSystem => Effet d'explosion quand la victime est détruite par la bombe
    /// </summary>
    [SerializeField] protected ParticleSystem mExplodeParticleSystem;

    /// <summary>
    /// GameObject mExclamation => Exclamation qui apparaît quand la victime détecte la bombe
    /// </summary>
    [SerializeField] protected GameObject mExclamation;

    /// <summary>
    /// eVictimState mVictimState => l'état actuel de la victime
    /// </summary>
    protected eVictimState mVictimState;

    /// <summary>
    /// NavMeshAgent mNavMeshAgent => NavMeshAgent attaché à la victime 
    /// </summary>
    protected UnityEngine.AI.NavMeshAgent mNavMeshAgent;

    /// <summary>
    /// GameObject[] mMovePoints => l'ensemble des points de promenade vers lesquels les victimes peuvent se diriger
    /// </summary>
    protected GameObject[] mMovePoints;

    /// <summary>
    /// float mUpdateTime => Temps interne de la victime se mettant à jours à chaque frame
    /// </summary>
    protected float mUpdateTime;

    /// <summary>
    /// float mUpdateTimeLimit => Temps limite de la victime au bout duquel elle passe de l'état WALK à WAIT et inversément
    /// </summary>
    protected float mUpdateTimeLimit;

    /// <summary>
    /// BBLevelScript mLevelScript => Référence vers le GameObject qui contrôles les paramètres du niveau
    /// </summary>
    protected BBLevelScript mLevelScript;

    /// <summary>
    /// Rigidbody mRigidbody => référence vers le Rigidbody qui compose la victime
    /// </summary>
    protected Rigidbody mRigidbody;

    /// <summary>
    /// float mDeltaTime => variable du deltatime 
    /// </summary>
    protected float mDeltaTime;


    /// <summary>
    /// Start() => initialisation de la victime :
    ///     - Désactivation de l'effet d'explosion de la victime
    ///     - Désactivation du point d'exclamation de la victime
    ///     - Récupération de la référence vers le NavMeshAgent de la victime
    ///     - Récupération de la référence vers le rigidbody de la victime
    ///     - Récupération de la référence vers le GameObject qui contrôle les paramètres du niveau
    ///     - Initialisation des Timer et de l'état initial de la victime
    /// </summary>
    protected void Start()
    {
        mExplodeParticleSystem.gameObject.SetActive(false);
        mExclamation.SetActive(false);

        mNavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        mMovePoints = GameObject.FindGameObjectsWithTag("SB_MovePoint");

        mVictimState = eVictimState.WALK;
        mUpdateTimeLimit = 3.5f;
        mUpdateTime = 0.0f;

        mLevelScript = GameObject.FindGameObjectWithTag("SB_LevelScript").GetComponent<BBLevelScript>();

        mRigidbody = GetComponent<Rigidbody>();

        transform.localScale = Vector3.zero;
        transform.DOScale(1.0f, 0.5f);

        mDeltaTime = Time.deltaTime;
    }

    /// <summary>
    /// walk() => controle la promenade de la victime
    /// </summary>
    protected void walk()
    {
        if (mUpdateTime >= mUpdateTimeLimit)
        {
            mVictimState = eVictimState.WAIT;
            mUpdateTime = 0.0f;
        }
        else
        {
            mUpdateTime += mDeltaTime;
        }

        mNavMeshAgent.Move(Vector3.zero);
    }

    /// <summary>
    /// wait() => Contrôle la phase d'attente de la victime
    /// </summary>
    protected void wait()
    {
        if (mUpdateTime >= mUpdateTimeLimit)
        {
            mVictimState = eVictimState.WALK;
            mNavMeshAgent.SetDestination(selectRandomMovePoint());
            mUpdateTime = 0.0f;
        }
        else
        {
            mUpdateTime += mDeltaTime;
        }
    }

    /// <summary>
    /// selectRandomMovePoint() => Permet à la victime de sélectionner sa prochaine destination de promenade
    /// </summary>
    /// <returns>Vector3 => Destination choisie aléatoirement</returns>
    private Vector3 selectRandomMovePoint()
    {
        return mMovePoints[Mathf.RoundToInt(Random.Range(0.0f, mMovePoints.Length - 1))].transform.position;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    virtual public void onBombDetectionStart(Vector3 bombPosition)
    {
    }

    virtual public void onBombDetectionStop()
    {
    }

    /// <summary>
    /// selfDestroy() => gère le processus de destruction de la victime :
    ///     - Activation de l'effet d'explosion et désactivation du point d'exclamation
    ///     - Désactivation du MeshRenderer et du Collider et du rigidbody
    ///     - Appelle via un Broadcast le champs de détection de la victime pour le désactiver
    ///     - Appelle la destruction de l'objet après un délai
    /// </summary>
    public void selfDestroy()
    {

        transform.DOKill();
        mExclamation.SetActive(false);

        mExplodeParticleSystem.gameObject.SetActive(true);
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        mRigidbody.constraints = RigidbodyConstraints.FreezeAll;

        DOVirtual.DelayedCall(1.0f, () =>
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
        });

        BroadcastMessage("disableBombDetection");
    }

}

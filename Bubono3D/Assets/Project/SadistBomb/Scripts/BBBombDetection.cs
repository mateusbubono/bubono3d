﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBBombDetection : MonoBehaviour
{
    /// <summary>
    /// BBVictim mVictim => référence vers la victime qui est parente de son détecteur de bombe
    /// </summary>
    [SerializeField] BBVictim mVictim;

    /// <summary>
    ///  OnTriggerEnter() => Lorsque le gameobject détecte une bombe il notifie la victime qui change alors son comportement
    /// </summary>
    /// <param name="other">Collider other => Objet de collision</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "SB_Bomb")
        {
            mVictim.onBombDetectionStart(other.gameObject.transform.position);
        }
    }

    /// <summary>
    /// OnTriggerExit() => Lorsque la bombe sort de la zone de détection de la bombe elle notifie la victime qui adapte son comportement
    /// </summary>
    /// <param name="other">>Collider other => Objet de collision</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "SB_Bomb")
        {
            mVictim.onBombDetectionStop();
        }
    }

    /// <summary>
    /// disableBombDetection() => la méthode est appelée depuis la victime lorsque la victime est détruite par une bombe
    /// </summary>
    private void disableBombDetection()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSController : MonoBehaviour
{
    [SerializeField] Joystick mJoystick;
    [SerializeField] ZSCharacter mCharacter;
    bool mRotateON = false;

    // Update is called once per frame
    void Update()
    {
        rotateShape();
    }

    private void rotateShape()
    {
        mCharacter.rotateShape(mJoystick.Horizontal);

        /*if(Input.GetMouseButtonDown(0))
        {
            mCharacter.rotateShape();
        }*/
    }
}

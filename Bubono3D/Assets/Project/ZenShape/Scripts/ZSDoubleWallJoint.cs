﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSDoubleWallJoint : MonoBehaviour
{
    [SerializeField] ZSDoubleWall[] mDoubleWallTab = new ZSDoubleWall[2];
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 2; i++)
        {
            mDoubleWallTab[i].runChooseRandomShape();
        }

        while(mDoubleWallTab[1].getShape() == mDoubleWallTab[0].getShape())
        {
            mDoubleWallTab[1].runChooseRandomShape();
        }
    }

            

}


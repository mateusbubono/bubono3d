﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSMovableObject : MonoBehaviour
{

    protected Rigidbody mRigidbody;
    [SerializeField] protected float mSpeed = 100.0f;

    // Start is called before the first frame update
    protected void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    protected void Update()
    {
        mRigidbody.AddForce(Vector3.back * Time.deltaTime * mSpeed);
    }
}

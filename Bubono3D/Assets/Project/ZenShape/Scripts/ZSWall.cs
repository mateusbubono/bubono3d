﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSWall : ZSMovableObject
{
    [SerializeField] protected GameObject[] mShapeTab;

    protected ZSGameParameters.eShape mShapePattern;

    // Start is called before the first frame update
    private new void Start()
    {
        base.Start();
        mShapePattern = chooseRandomShape(mShapeTab);
    }

    protected ZSGameParameters.eShape chooseRandomShape(GameObject[] shapeTab)
    {
        int randomShape = Mathf.RoundToInt(Random.Range(0.0f, (float)ZSGameParameters.eShape.TRIANGLE));
        Debug.Log(randomShape);
        for(int i = 0; i< shapeTab.Length; i++)
        {
            if(i != randomShape)
                shapeTab[i].gameObject.SetActive(false);
        }

        return (ZSGameParameters.eShape)randomShape;
    }

    public bool compareShape(ZSGameParameters.eShape shape)
    {
        return shape == mShapePattern;
    }

}

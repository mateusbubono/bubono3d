﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZSGameManager : MonoBehaviour
{

    [SerializeField] Text mScoreText;
    int mScore;

    // Start is called before the first frame update
    void Start()
    {
        increaseScore(0);
    }

    public void onWallDestroyed()
    {
        increaseScore((int)ZSGameParameters.eGameScore.WALL);
    }

    public void onCharacterShoot()
    {
        increaseScore((int)ZSGameParameters.eGameScore.SHOOTABLECHARACTER);
    }

    private void increaseScore(int scoreAmount)
    {
        mScore += scoreAmount;
        mScoreText.text = mScore.ToString();
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSWallCollector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ZS_Wall")
        {
            Destroy(other.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSWallSpawner : MonoBehaviour
{
    [SerializeField] float mSpawnSpeed = 5.0f;
    [SerializeField] GameObject[] mSpawnTable;

    private void Start()
    {
        InvokeRepeating("spawn", mSpawnSpeed, mSpawnSpeed);
    }

    private void spawn()
    {
        int randomInt = Mathf.RoundToInt(Random.Range(0.0f, mSpawnTable.Length - 1.0f));

        GameObject spawnObject = mSpawnTable[randomInt];
        if(spawnShootableCharacter(spawnObject.GetComponent<ZSShootableCharacter>()) == false)
            spawnWall(spawnObject);
    }

    private bool spawnShootableCharacter(ZSShootableCharacter shootableCharacter)
    {
        if(shootableCharacter != null)
        {
            int randomPosition = Mathf.RoundToInt(Random.Range(0.0f, transform.childCount - 1.0f));
            Debug.Log(randomPosition);
            Instantiate(shootableCharacter.gameObject, transform.GetChild(randomPosition).transform.position, transform.GetChild(randomPosition).transform.rotation);

            return true;
        }

        return false;
    }

    private void spawnWall(GameObject wall)
    {
        Instantiate(wall, transform.position, transform.rotation);
    }



}

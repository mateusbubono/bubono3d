﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSGameParameters : MonoBehaviour
{
    public enum eShape
    {
        CUBE,
        SPHERE,
        TRIANGLE,
        SHAPENUMBER
    };

    public enum eGameScore
    {
        WALL = 100,
        DOUBLEWALL = 200,
        SHOOTABLECHARACTER = 400
    };
}

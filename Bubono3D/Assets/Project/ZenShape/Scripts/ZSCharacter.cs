﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSCharacter : MonoBehaviour
{

    [SerializeField] GameObject mRootPoint;

    [SerializeField] Vector3 mRotateAmount = new Vector3(0.0f, 200.0f, 0.0f);


    public void rotateShape(float joystickSpeed)
    {
        float rotateSpeed = joystickSpeed * Time.deltaTime;
        mRootPoint.transform.Rotate(mRotateAmount * rotateSpeed);
    }
}

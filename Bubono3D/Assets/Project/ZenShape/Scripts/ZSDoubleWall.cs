﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSDoubleWall : ZSWall
{
    new void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
    }

    public ZSGameParameters.eShape getShape()
    {
        return mShapePattern;
    }

    public void runChooseRandomShape()
    {
        foreach(GameObject shape in mShapeTab)
        {
            shape.SetActive(true);
        }

        mShapePattern = chooseRandomShape(mShapeTab);
    }

}

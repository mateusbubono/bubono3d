﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZSShape : MonoBehaviour
{

    [SerializeField] ZSGameParameters.eShape mShape;

    ZSGameManager mGameManager;

    // Start is called before the first frame update
    void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ZSGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch(collision.gameObject.tag)
        {
            case "ZS_Wall":
                ZSWall wall = collision.gameObject.GetComponent<ZSWall>();
                crossWall(wall);
                break;
            case "ZS_DoubleWall":
                ZSDoubleWall doubleWall = collision.gameObject.GetComponent<ZSDoubleWall>();
                crossWall(doubleWall);
                break;
            case "ZS_ShootableCharacter":
                shootCharacter(collision.gameObject.GetComponent<ZSShootableCharacter>());
                break;
        }
    }

    private void crossWall(ZSWall wall)
    {
        if (wall.compareShape(mShape) == true)
        {
            Destroy(wall.gameObject);
            mGameManager.onWallDestroyed();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void shootCharacter(ZSShootableCharacter shootableCharacter)
    {
        if(shootableCharacter != null)
        {
            Destroy(shootableCharacter.gameObject);
            mGameManager.onCharacterShoot();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCItemDistributor : TCItemManager
{

    private void OnTriggerEnter(Collider other)
    {
       if(other.tag == "TC_Character" && mItems.Count > 0)
       {
            TCCharacter character = other.gameObject.GetComponent<TCCharacter>();
            if(mTeamNumber == character.mTeamNumber)
            {
                giveItemToCharacter(character);
            }
       }
    }

    private void giveItemToCharacter(TCCharacter character)
    {
        if(character.getItemFromDistributor(mItems[0]))
            mItems.RemoveAt(0);
    }


}

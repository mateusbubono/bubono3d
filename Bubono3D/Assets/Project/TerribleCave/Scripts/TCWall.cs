﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCWall : MonoBehaviour
{

    private bool mIsHidden = false;
    Material mMaterial;

    private void Start()
    {
        mMaterial = GetComponent<Renderer>().material;
    }

    public void show()
    {
        if(mIsHidden)
        {
            Color color = mMaterial.color;
            color.a = 1.0f;
            mIsHidden = false;
            mMaterial.color = color;
        }
    }

    public void hide()
    {
        if (!mIsHidden)
        {
            Color color = mMaterial.color;
            color.a = 0.25f;
            mIsHidden = true;
            mMaterial.color = color;
        }
    }
}

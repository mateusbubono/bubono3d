﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCCharacter : MonoBehaviour
{
    public bool mAIControlON = false;
    public float mSpeed = 900.0f;
    public GameObject mItemSpace;
    public int mTeamNumber;
    public GameObject mCurrentItem;
    public Rigidbody mRigidbody;

    public TCGameManager mGameManager;

    public GameObject mCharacterModel;
    public Animator mAnimation;

    [SerializeField] GameObject mNoRotatingChildren;

    // Start is called before the first frame update
    void Start()
    {
        mCurrentItem = null;
        mRigidbody = GetComponent<Rigidbody>();

        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<TCGameManager>();

        mAnimation = mCharacterModel.GetComponent<Animator>();
    }

    private void Update()
    {
        if(mNoRotatingChildren != null)
            mNoRotatingChildren.transform.rotation = Quaternion.identity;
    }

    public void move(Vector3 direction)
    {

        if(direction != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(direction);
            mRigidbody.AddForce(direction * Time.deltaTime * mSpeed);
            mAnimation.SetFloat("mSpeed", 2.0f);
        }
        else
        {
            mAnimation.SetFloat("mSpeed", 0.0f);
        }
    }

    public bool getItemFromDistributor(GameObject item)
    {
        if(mCurrentItem == null)
        {
            item.transform.SetParent(transform);
            item.transform.position = mItemSpace.transform.position;
            item.transform.rotation = mItemSpace.transform.rotation;

            mCurrentItem = item;
            return true;
        }

        return false;
    }

    public GameObject giveItemToCollector()
    {
        if (mCurrentItem != null)
        {
            mGameManager.onItemCollected(GetComponent<TCCharacter>());
            mCurrentItem.GetComponent<BoxCollider>().enabled = false;
            mCurrentItem.transform.parent = null;
            return mCurrentItem;
        }
        return null;
    }

    public void removeCurrentItem()
    {
        mCurrentItem = null;
    }

    public void rejectCurrentItem()
    {
        if(mCurrentItem != null)
        {
            mCurrentItem.GetComponent<BoxCollider>().enabled = true;
            mCurrentItem.transform.parent = null;
            mCurrentItem = null;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "TC_Item" && mCurrentItem == null)
        {
            mCurrentItem = other.gameObject;
            mCurrentItem.transform.parent = transform;
            mCurrentItem.transform.position = mItemSpace.transform.position;
        }
    }

}

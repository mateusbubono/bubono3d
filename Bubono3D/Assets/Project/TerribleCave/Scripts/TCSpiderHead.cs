﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCSpiderHead : MonoBehaviour
{
    [SerializeField] TCSpider mSpider;

    private void OnTriggerEnter(Collider other)
    {
        mSpider.onCharacterEaten(other.gameObject);
    }
}

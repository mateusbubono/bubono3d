﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCPlayerController : TCController
{
    [SerializeField] Joystick mJoystick;

    private void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<TCGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mGameManager == null || mGameManager.mGameState != TCGameManager.eGameState.INGAME)
            return;
        move();
    }

    private void move()
    {
        Vector3 direction = new Vector3(mJoystick.Direction.x, 0, mJoystick.Direction.y);
        mPlayerCharacter.move(direction);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TCAIController : MonoBehaviour
{

    public enum eAIState
    {
        WAIT,
        COLLECT,
        DROPDOWN,
        FLEE,
        CHASE
    }

    public eAIState mAIState = eAIState.WAIT;
    private TCItemCollector mOwnItemCollector;
    private TCItemDistributor mOwnItemDistributor;
    [SerializeField] TCCharacter mCharacter;

    private NavMeshAgent mNavMeshAgent;
    private GameObject mSpider;

    private bool stopFleeON;

    private TCGameManager mGameManager;

    private GameObject[] mFleePoints;

    private TCCharacter mMainCharacter;
    

    // Start is called before the first frame update
    void Start()
    {
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<TCGameManager>();
        stopFleeON = false;
        mNavMeshAgent = GetComponent<NavMeshAgent>();
        mFleePoints = GameObject.FindGameObjectsWithTag("TC_FleePoint");

        searchMainCharacter();

        if (mCharacter.mAIControlON)
        {
            searchItemDistributor();

            searchItemCollector();

            setAIAgentToCollect();
        }
        else
        {
            mNavMeshAgent.enabled = false;
        }
    }

    private void searchMainCharacter()
    {
        GameObject[] charactersList = GameObject.FindGameObjectsWithTag("TC_Character");

        foreach(GameObject characterGO in charactersList)
        {
            TCCharacter character = characterGO.GetComponent<TCCharacter>();
            if(character.mAIControlON == false)
            {
                mMainCharacter = character;
                break;
            }
        }
    }

    private void Update()
    {
        if (mGameManager == null || mGameManager.mGameState != TCGameManager.eGameState.INGAME)
            return;

        if(mAIState == eAIState.DROPDOWN || mAIState == eAIState.COLLECT)
        {
            mNavMeshAgent.SetDestination(mMainCharacter.transform.position);
        }

        /*if(mCharacter.mAIControlON && mAIState > eAIState.WAIT)
        {
            switch(mAIState)
            {
                case eAIState.COLLECT :
                case eAIState.DROPDOWN :
                    mNavMeshAgent.Move(Vector3.zero);
                    break;
                case eAIState.FLEE:
                    moveToFlee();
                    break;
            }
            
        }*/
    }

    private void searchItemDistributor()
    {
        GameObject[] itemDistribList = GameObject.FindGameObjectsWithTag("TC_ItemDistributor");

        foreach (GameObject itemDistributorGO in itemDistribList)
        {
            TCItemDistributor itemDistributor = itemDistributorGO.GetComponent<TCItemDistributor>();
            if (itemDistributor.mTeamNumber == mCharacter.mTeamNumber)
            {
                mOwnItemDistributor = itemDistributor;
                break;
            }
        }
    }

    private void searchItemCollector()
    {
        GameObject[] itemCollectorList = GameObject.FindGameObjectsWithTag("TC_ItemCollector");
        foreach (GameObject itemCollectorGO in itemCollectorList)
        {
            TCItemCollector itemCollector = itemCollectorGO.GetComponent<TCItemCollector>();
            if (itemCollector.mTeamNumber == mCharacter.mTeamNumber)
            {
                mOwnItemCollector = itemCollector;
                break;
            }
        }
    }

    private void setAIAgentToCollect()
    {
        //mNavMeshAgent.enabled = true;
        mAIState = eAIState.COLLECT;
        mNavMeshAgent.SetDestination(mOwnItemDistributor.transform.position);
        mCharacter.mAnimation.SetFloat("mSpeed", 2.0f);
    }

    private void setAIAgentToDropDown()
    {
        //mNavMeshAgent.enabled = true;
        mAIState = eAIState.DROPDOWN;
        mNavMeshAgent.SetDestination(mOwnItemCollector.transform.position);
        mNavMeshAgent.speed = 7f;
        mCharacter.mAnimation.SetFloat("mSpeed", 2.0f);
    }

    private void setAIAgentToFollowMainCharacter()
    {
        mAIState = eAIState.DROPDOWN;
        mNavMeshAgent.SetDestination(mMainCharacter.transform.position);
        mNavMeshAgent.speed = 7f;
        mCharacter.mAnimation.SetFloat("mSpeed", 2.0f);
    }

    private void selectFleePoint()
    {
        foreach(GameObject fleePoint in mFleePoints)
        {
            Vector3 fleePointDir = fleePoint.transform.position - transform.position;

            if (Vector3.Dot(fleePointDir, transform.forward) < 0)
            {
                mNavMeshAgent.SetDestination(fleePoint.transform.position);
                mCharacter.mAnimation.SetFloat("mSpeed", 2.0f);
            }
        }
    }

    private void setAIAgentToFlee(GameObject spider)
    {
        //mNavMeshAgent.enabled = false;
        mSpider = spider;
        mNavMeshAgent.speed = 9.0f;
        mAIState = eAIState.FLEE;
        selectFleePoint();
    }

    private void moveToFlee()
    {
        Vector3 spiderDirection =  mCharacter.transform.position - mSpider.transform.position;
        mCharacter.transform.rotation = Quaternion.LookRotation(spiderDirection);
        mCharacter.mRigidbody.AddForce(spiderDirection * Time.deltaTime * mCharacter.mSpeed);
        //mCharacter.transform.Translate(Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TC_ItemDistributor" && other.GetComponent<TCItemManager>().mTeamNumber == mCharacter.mTeamNumber && mAIState == eAIState.COLLECT  && mCharacter.mCurrentItem != null)
        {
            //setAIAgentToDropDown();
            setAIAgentToFollowMainCharacter();
        }
        else if(other.tag == "TC_ItemCollector" && other.GetComponent<TCItemManager>().mTeamNumber == mCharacter.mTeamNumber && mAIState == eAIState.DROPDOWN)
        {
            //setAIAgentToCollect();
            setAIAgentToFollowMainCharacter();
        }
        else if(other.tag == "TC_Spider")
        {
            //setAIAgentToFlee(other.gameObject);
        }
        else if(other.tag == "TC_FleePoint")
        {
            fromFleeToWalk();
        }
        else if(other.tag == "TC_Item" )
        {
            mCharacter.mCurrentItem = other.gameObject;
            mCharacter.mCurrentItem.transform.parent = transform;
            mCharacter.mCurrentItem.transform.position = mCharacter.mItemSpace.transform.position;
        }
        /*else
        {
            mAIState = eAIState.WAIT;
        }*/
    }

    /*private void OnTriggerExit(Collider other)
    {
        if (other.tag == "TC_Spider" && !stopFleeON)
        {
            stopFleeON = true;
            Invoke("fromFleeToWalk", 1.0f);
        }
    }*/

    private void fromFleeToWalk()
    {
        stopFleeON = false;
        if (mCharacter.mCurrentItem == null)
        {
            setAIAgentToCollect();
        }
        else
        {
            setAIAgentToDropDown();
        }
    }

}

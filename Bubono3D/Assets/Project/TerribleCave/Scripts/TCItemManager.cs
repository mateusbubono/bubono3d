﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCItemManager : MonoBehaviour
{
    [SerializeField] protected List<GameObject> mItems;
    [SerializeField] public int mTeamNumber;

    [SerializeField] protected GameObject mIcon;

}

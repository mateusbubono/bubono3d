﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TCHUDTarget : MonoBehaviour
{

    [SerializeField] GameObject mArrow;
    [SerializeField] Image mImageArrow;
    [SerializeField] GameObject mTargetGameObject;
    [SerializeField] TCCharacter mMainCharacter;

    private Quaternion mStartArrowRotation;

    // Start is called before the first frame update
    void Start()
    {
        if(mTargetGameObject == null)
        {
            mArrow.SetActive(false);
            return;
        }

        mStartArrowRotation = mArrow.transform.rotation;

        GameObject[] characterList = GameObject.FindGameObjectsWithTag("TC_Character");
        foreach(GameObject characterGO in characterList)
        {
            TCCharacter character = characterGO.GetComponent<TCCharacter>();
            if(character.mAIControlON == false)
            {
                mMainCharacter = character;
                break;
            }
        }
    }

    private void LateUpdate()
    {
        if (mTargetGameObject == null)
            return;

        Vector3 targetPos = Camera.main.WorldToScreenPoint(mTargetGameObject.transform.position + new Vector3(0.0f, 3.0f, 0.0f));
        transform.position = targetPos;

        if (isVisibleToCamera(mTargetGameObject.transform))
        {
            mArrow.transform.rotation = mStartArrowRotation;
        }
        else
        {
            clampToWindow(GetComponent<RectTransform>(), transform.parent.GetComponent<RectTransform>());

            Vector3 tg = Camera.main.WorldToScreenPoint(mTargetGameObject.transform.position);
            Vector3 center = Camera.main.WorldToScreenPoint(mMainCharacter.transform.position);

            Vector3 direction = (tg - center).normalized;
            mArrow.transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
        }

    }

    public bool isVisibleToCamera(Transform transform)
    {
        Vector3 visTest = Camera.main.WorldToViewportPoint(transform.position);
        return (visTest.x >= 0 && visTest.y >= 0) && (visTest.x <= 1 && visTest.y <= 1) && visTest.z >= 0;
    }

    void clampToWindow(RectTransform panelRectTransform, RectTransform parentRectTransform)
    {
        Vector3 pos = panelRectTransform.localPosition;

        Vector3 minPosition = parentRectTransform.rect.min - panelRectTransform.rect.min;
        Vector3 maxPosition = parentRectTransform.rect.max - panelRectTransform.rect.max;

        pos.x = Mathf.Clamp(panelRectTransform.localPosition.x, minPosition.x, maxPosition.x);
        pos.y = Mathf.Clamp(panelRectTransform.localPosition.y, minPosition.y, maxPosition.y);

        panelRectTransform.localPosition = pos;
    }
}

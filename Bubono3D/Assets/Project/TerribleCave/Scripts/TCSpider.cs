﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TCSpider : MonoBehaviour
{

    enum eAIState
    {
        PATROL,
        CHASE
    }

    private eAIState mAIState;
    private NavMeshAgent mNavMeshAgent;

    public List<GameObject> mItemManagerList;

    private GameObject mVictimToChase;
    private GameObject mPatrolPoint;

    private Rigidbody mRigidbody;

    private TCGameManager mGameManager;

    // Start is called before the first frame update
    void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mNavMeshAgent = GetComponent<NavMeshAgent>();
        mGameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<TCGameManager>();

        findAllMovePoints();

        setSpiderMoveToPatrol();
    }

    private void findAllMovePoints()
    {
        mItemManagerList = new List<GameObject>();

        GameObject[] itemManagerTabs = GameObject.FindGameObjectsWithTag("TC_ItemCollector");
        foreach (GameObject itemManager in itemManagerTabs)
        {
            mItemManagerList.Add(itemManager);
        }

        itemManagerTabs = GameObject.FindGameObjectsWithTag("TC_ItemDistributor");
        foreach (GameObject itemManager in itemManagerTabs)
        {
            mItemManagerList.Add(itemManager);
        }

        itemManagerTabs = GameObject.FindGameObjectsWithTag("TC_FleePoint");
        foreach (GameObject itemManager in itemManagerTabs)
        {
            mItemManagerList.Add(itemManager);
        }
    }

    private void setSpiderMoveToPatrol()
    {
        mAIState = eAIState.PATROL;
        //mNavMeshAgent.enabled = true;

        int randomIndex = Random.Range(0, mItemManagerList.Count);
        mNavMeshAgent.speed = 3.5f;
        mNavMeshAgent.SetDestination(mItemManagerList[randomIndex].transform.position);
        mPatrolPoint = mItemManagerList[randomIndex];

    }

    private void setSpiderMoveToChase(GameObject victim)
    {
        //mNavMeshAgent.enabled = false;
        mAIState = eAIState.CHASE;
        mVictimToChase = victim;
        mNavMeshAgent.speed = 7.0f;
        mNavMeshAgent.SetDestination(victim.transform.position);
    }

    private void chaseMove()
    {
        Vector3 victimDirection = mVictimToChase.transform.position - transform.position;
        transform.rotation = Quaternion.LookRotation(victimDirection);
        //transform.Translate(Vector3.forward * Time.deltaTime * 5.0f);
        mRigidbody.AddForce(victimDirection * Time.deltaTime * 400.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (mGameManager == null || mGameManager.mGameState != TCGameManager.eGameState.INGAME)
            return;
        /*switch(mAIState)
        {
            case eAIState.PATROL:
                mNavMeshAgent.SetDestination(mPatrolPoint.transform.position);
                mNavMeshAgent.Move(Vector3.zero);
                break;
            case eAIState.CHASE:
                chaseMove();
                break;
        }*/
    }

    private void adjustBehavior(Collider other)
    {
        if (other.tag == "TC_ItemCollector" || other.tag == "TC_ItemDistributor" || other.tag == "TC_FleePoint")
        {
            if (mPatrolPoint == other.gameObject)
                setSpiderMoveToPatrol();
        }
        else if (other.tag == "TC_Character")
        {
            setSpiderMoveToChase(other.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        adjustBehavior(other);
    }

    private void OnTriggerStay(Collider other)
    {
        adjustBehavior(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "TC_Character")
        {
            setSpiderMoveToPatrol();
        }
    }

    public void onCharacterEaten(GameObject character)
    {
        if (character.tag == "TC_Character")
        {
            mGameManager.onCharacterEaten(character.GetComponent<TCCharacter>());

            TCAIController aiController = character.GetComponent<TCAIController>();
            if (aiController != null )
                aiController.enabled = false;

            NavMeshAgent navMesAgent = character.GetComponent<NavMeshAgent>();
            if(navMesAgent != null)
                navMesAgent.enabled = false;

            TCCharacter tcCharacter = character.GetComponent<TCCharacter>();
            if (tcCharacter.mAIControlON)
                character.SetActive(false);
            else
                tcCharacter.mCharacterModel.SetActive(false);
            setSpiderMoveToPatrol();

            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCCamera : MonoBehaviour
{
    [SerializeField] TCCharacter mCharacter;
    LayerMask mLayerMask = 0;
    RaycastHit[] mHits;

    // Start is called before the first frame update
    void Start()
    {
        mLayerMask = LayerMask.GetMask("TC_Wall");
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, mCharacter.transform.position);
        Vector3 direction = (mCharacter.transform.position - transform.position).normalized * distance;

        RaycastHit[] hits = Physics.RaycastAll(transform.position, direction, distance, mLayerMask, QueryTriggerInteraction.Collide);
        
        if(mHits != null)
        {
            foreach (RaycastHit hit in mHits)
            {
                hit.transform.gameObject.GetComponent<TCWall>().show();
            }
        }
        

        foreach(RaycastHit hit in hits)
        {
            hit.transform.gameObject.GetComponent<TCWall>().hide();
        }

        mHits = hits;
    }
}

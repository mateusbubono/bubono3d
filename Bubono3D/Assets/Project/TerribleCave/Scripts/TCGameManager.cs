﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TCGameManager : MonoBehaviour
{
    public enum eGameState
    {
        START,
        INGAME,
        GAMEOVER
    }

    public eGameState mGameState = eGameState.START;
    int mGoal;
    [SerializeField] TextMesh mWinText;
    public Dictionary<TCCharacter, int> mPlayerDict;

    [SerializeField] GameObject[] mLevels;
    [SerializeField] int mCurrentLevel = -1;

    [SerializeField] GameObject mPlane;

    int mPlayerCount = 0;

    // Start is called before the first frame update
    void Awake()
    {
        mGameState = eGameState.INGAME;
        loadLevel();
        mPlayerDict = new Dictionary<TCCharacter, int>();
        GameObject[] characterListGO = GameObject.FindGameObjectsWithTag("TC_Character");
        mWinText.gameObject.SetActive(false);

        foreach (GameObject characterGO in characterListGO)
        {
            mPlayerDict.Add(characterGO.GetComponent<TCCharacter>(), 0);
        }

        GameObject.FindGameObjectWithTag("Plane").GetComponent<UnityEngine.AI.NavMeshSurface>().BuildNavMesh();

        mGoal = GameObject.FindGameObjectWithTag("Level").GetComponent<TCLevel>().mGoal;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
            restart();
            
    }

    void loadLevel()
    {
        if (mCurrentLevel == -1)
        {
            mCurrentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        }
        GameObject level = Instantiate(mLevels[mCurrentLevel]);
        level.transform.parent = transform;

    }

    public void nextLevel()
    {
        mCurrentLevel = (mCurrentLevel + 1) % mLevels.Length;
        PlayerPrefs.SetInt("currentLevel", mCurrentLevel);
        restart();
    }

    private void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void onItemCollected(TCCharacter character)
    {
        mPlayerCount++;
        if(mPlayerCount >= mGoal)
        {
            gameOver(character);
        }
    }

    public void onCharacterEaten(TCCharacter character)
    {
        if(character.mAIControlON == false)
        {
            mGameState = eGameState.GAMEOVER;
            fail();
        }
        else
        {
            character.rejectCurrentItem();
        }
        /*else
        {
            mPlayerDict.Remove(character);
            if(mPlayerDict.Count == 1)
            {
                mGameState = eGameState.GAMEOVER;
                win();
            }
        }*/
    }

    private void gameOver(TCCharacter character)
    {
        mGameState = eGameState.GAMEOVER;
        if(character.mTeamNumber == 0)
        {
            win();
        }
        else
        {
            fail();
        }
    }

    private void win()
    {
        mWinText.text = "You Win !!!";
        mWinText.gameObject.SetActive(true);

        Invoke("nextLevel", 2.0f);
    }

    private void fail()
    {
        mWinText.text = "You Lose !!!";
        mWinText.gameObject.SetActive(true);

        Invoke("restart", 2.0f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCItemCollector : TCItemManager
{

    private List<GameObject> mStoredItems;
    [SerializeField] GameObject mItemsSpacesRoot;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TC_Character" && mStoredItems.Count < mItems.Count)
        {
            TCCharacter character = other.gameObject.GetComponent<TCCharacter>();
            if (mTeamNumber == character.mTeamNumber)
            {
                takeItemFromCharacter(character);
            }
        }
    }

    private void Start()
    {
        mStoredItems = new List<GameObject>();
    }

    private void takeItemFromCharacter(TCCharacter character)
    {
        GameObject item = character.giveItemToCollector();
        if(item != null)
        {
            character.removeCurrentItem();
            mStoredItems.Add(item);
            int currentIndex = mStoredItems.Count - 1;
            mStoredItems[currentIndex].transform.position = mItems[currentIndex].transform.position;
            mStoredItems[currentIndex].transform.rotation = mItems[currentIndex].transform.rotation;
            mStoredItems[currentIndex].transform.SetParent(mItemsSpacesRoot.transform);
        }
    }
}
